/*
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

var apigClientFactory = {};
apigClientFactory.newClient = function (config) {
    var apigClient = { };
    if(config === undefined) {
        config = {
            accessKey: '',
            secretKey: '',
            sessionToken: '',
            region: '',
            apiKey: undefined,
            defaultContentType: 'application/json',
            defaultAcceptType: 'application/json'
        };
    }
    if(config.accessKey === undefined) {
        config.accessKey = '';
    }
    if(config.secretKey === undefined) {
        config.secretKey = '';
    }
    if(config.apiKey === undefined) {
        config.apiKey = '';
    }
    if(config.sessionToken === undefined) {
        config.sessionToken = '';
    }
    if(config.region === undefined) {
        config.region = 'us-east-1';
    }
    //If defaultContentType is not defined then default to application/json
    if(config.defaultContentType === undefined) {
        config.defaultContentType = 'application/json';
    }
    //If defaultAcceptType is not defined then default to application/json
    if(config.defaultAcceptType === undefined) {
        config.defaultAcceptType = 'application/json';
    }

    
    // extract endpoint and path from url
    var invokeUrl = 'https://cd3rnj4rtc.execute-api.us-west-2.amazonaws.com/dev';
    var endpoint = /(^https?:\/\/[^\/]+)/g.exec(invokeUrl)[1];
    var pathComponent = invokeUrl.substring(endpoint.length);

    var sigV4ClientConfig = {
        accessKey: config.accessKey,
        secretKey: config.secretKey,
        sessionToken: config.sessionToken,
        serviceName: 'execute-api',
        region: config.region,
        endpoint: endpoint,
        defaultContentType: config.defaultContentType,
        defaultAcceptType: config.defaultAcceptType
    };

    var authType = 'NONE';
    if (sigV4ClientConfig.accessKey !== undefined && sigV4ClientConfig.accessKey !== '' && sigV4ClientConfig.secretKey !== undefined && sigV4ClientConfig.secretKey !== '') {
        authType = 'AWS_IAM';
    }

    var simpleHttpClientConfig = {
        endpoint: endpoint,
        defaultContentType: config.defaultContentType,
        defaultAcceptType: config.defaultAcceptType
    };

    var apiGatewayClient = apiGateway.core.apiGatewayClientFactory.newClient(simpleHttpClientConfig, sigV4ClientConfig);
    
    
    
    apigClient.apiBaseApiOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var apiBaseApiOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/BaseApi').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiBaseApiOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityApproveRejectReservationPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityApproveRejectReservationPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/approveRejectReservation').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityApproveRejectReservationPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityApproveRejectReservationOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityApproveRejectReservationOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/approveRejectReservation').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityApproveRejectReservationOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityDecriptActivityDetailTokenPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityDecriptActivityDetailTokenPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/decriptActivityDetailToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityDecriptActivityDetailTokenPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityDecriptActivityDetailTokenOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityDecriptActivityDetailTokenOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/decriptActivityDetailToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityDecriptActivityDetailTokenOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityLastStagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityLastStagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityLastStage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityLastStagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityLastStageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityLastStageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityLastStage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityLastStageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityNotificationsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityNotificationsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsAllPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityNotificationsAllPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsAll').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsAllPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsAllOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityNotificationsAllOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsAll').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsAllOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsByIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityNotificationsByIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsByIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsByIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityNotificationsByIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsByIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsOneItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityNotificationsOneItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsOneItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsOneItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsOneItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityNotificationsOneItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsOneItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsOneItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsRentalPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityNotificationsRentalPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsRentalPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsRentalOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityNotificationsRentalOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsRentalOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsRentalSectionsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityNotificationsRentalSectionsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsRentalSections').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsRentalSectionsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsRentalSectionsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityNotificationsRentalSectionsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsRentalSections').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsRentalSectionsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsSystemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserActivityNotificationsSystemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsSystem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsSystemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserActivityNotificationsSystemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserActivityNotificationsSystemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserActivityNotificationsSystem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserActivityNotificationsSystemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserTotalStatsActivityPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivityGetUserTotalStatsActivityPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserTotalStatsActivity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserTotalStatsActivityPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivityGetUserTotalStatsActivityOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivityGetUserTotalStatsActivityOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/getUserTotalStatsActivity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivityGetUserTotalStatsActivityOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivitySetOpenedUserNotificationPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivitySetOpenedUserNotificationPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/setOpenedUserNotification').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivitySetOpenedUserNotificationPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivitySetOpenedUserNotificationOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivitySetOpenedUserNotificationOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/setOpenedUserNotification').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivitySetOpenedUserNotificationOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivitySetUserOpenedNotificationsRentalPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivitySetUserOpenedNotificationsRentalPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/setUserOpenedNotificationsRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivitySetUserOpenedNotificationsRentalPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivitySetUserOpenedNotificationsRentalOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivitySetUserOpenedNotificationsRentalOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/setUserOpenedNotificationsRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivitySetUserOpenedNotificationsRentalOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivitySetUserOpenedNotificationsSystemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiActivitySetUserOpenedNotificationsSystemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/setUserOpenedNotificationsSystem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivitySetUserOpenedNotificationsSystemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiActivitySetUserOpenedNotificationsSystemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiActivitySetUserOpenedNotificationsSystemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/activity/setUserOpenedNotificationsSystem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiActivitySetUserOpenedNotificationsSystemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCallbackShuftiproPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var apiCallbackShuftiproPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/callback/shuftipro').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCallbackShuftiproPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityCreateUpadateCommunityPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityCreateUpadateCommunityPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/createUpadateCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityCreateUpadateCommunityPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityCreateUpadateCommunityOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityCreateUpadateCommunityOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/createUpadateCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityCreateUpadateCommunityOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityCreateUpdateCommunityItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityCreateUpdateCommunityItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/createUpdateCommunityItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityCreateUpdateCommunityItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityCreateUpdateCommunityItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityCreateUpdateCommunityItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/createUpdateCommunityItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityCreateUpdateCommunityItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityCreateUpdateCommunityMemberPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityCreateUpdateCommunityMemberPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/createUpdateCommunityMember').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityCreateUpdateCommunityMemberPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityCreateUpdateCommunityMemberOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityCreateUpdateCommunityMemberOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/createUpdateCommunityMember').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityCreateUpdateCommunityMemberOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetCommunityItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetCommunityItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityItemsOwnPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetCommunityItemsOwnPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityItemsOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityItemsOwnPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityItemsOwnOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetCommunityItemsOwnOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityItemsOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityItemsOwnOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetCommunityMembersPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembers').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetCommunityMembersOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembers').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersOwnPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetCommunityMembersOwnPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembersOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersOwnPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersOwnOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetCommunityMembersOwnOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembersOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersOwnOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersTopTenPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetCommunityMembersTopTenPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembersTopTen').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersTopTenPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersTopTenOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetCommunityMembersTopTenOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembersTopTen').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersTopTenOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersTopTenOwnPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetCommunityMembersTopTenOwnPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembersTopTenOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersTopTenOwnPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityMembersTopTenOwnOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetCommunityMembersTopTenOwnOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityMembersTopTenOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityMembersTopTenOwnOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityUserItemsToAddPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetCommunityUserItemsToAddPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityUserItemsToAdd').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityUserItemsToAddPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetCommunityUserItemsToAddOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetCommunityUserItemsToAddOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getCommunityUserItemsToAdd').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetCommunityUserItemsToAddOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetMyCommunitiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetMyCommunitiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getMyCommunities').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetMyCommunitiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetMyCommunitiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetMyCommunitiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getMyCommunities').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetMyCommunitiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetMyCommunitiesBelongPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityGetMyCommunitiesBelongPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getMyCommunitiesBelong').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetMyCommunitiesBelongPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityGetMyCommunitiesBelongOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityGetMyCommunitiesBelongOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/getMyCommunitiesBelong').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityGetMyCommunitiesBelongOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityRemoveItemCommunityPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityRemoveItemCommunityPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/removeItemCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityRemoveItemCommunityPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityRemoveItemCommunityOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityRemoveItemCommunityOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/removeItemCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityRemoveItemCommunityOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityRemoveMemeberCommunityPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunityRemoveMemeberCommunityPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/removeMemeberCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityRemoveMemeberCommunityPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunityRemoveMemeberCommunityOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunityRemoveMemeberCommunityOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/removeMemeberCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunityRemoveMemeberCommunityOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunitySetCommunityMemberCanAddPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunitySetCommunityMemberCanAddPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/setCommunityMemberCanAdd').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunitySetCommunityMemberCanAddPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunitySetCommunityMemberCanAddOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunitySetCommunityMemberCanAddOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/setCommunityMemberCanAdd').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunitySetCommunityMemberCanAddOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunitySetVisibleItemCommunityPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiCommunitySetVisibleItemCommunityPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/setVisibleItemCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunitySetVisibleItemCommunityPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiCommunitySetVisibleItemCommunityOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiCommunitySetVisibleItemCommunityOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/community/setVisibleItemCommunity').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiCommunitySetVisibleItemCommunityOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryCategoryItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiDiscoveryGetDiscoveryCategoryItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryCategoryItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryCategoryItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryCategoryItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiDiscoveryGetDiscoveryCategoryItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryCategoryItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryCategoryItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryFeaturedItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiDiscoveryGetDiscoveryFeaturedItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryFeaturedItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryFeaturedItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryFeaturedItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiDiscoveryGetDiscoveryFeaturedItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryFeaturedItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryFeaturedItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryFeaturedItemsCountPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiDiscoveryGetDiscoveryFeaturedItemsCountPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryFeaturedItemsCount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryFeaturedItemsCountPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryFeaturedItemsCountOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiDiscoveryGetDiscoveryFeaturedItemsCountOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryFeaturedItemsCount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryFeaturedItemsCountOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryItemsMapPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiDiscoveryGetDiscoveryItemsMapPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryItemsMap').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryItemsMapPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryItemsMapOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiDiscoveryGetDiscoveryItemsMapOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryItemsMap').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryItemsMapOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryItemsMapNewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiDiscoveryGetDiscoveryItemsMapNewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryItemsMapNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryItemsMapNewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryItemsMapNewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiDiscoveryGetDiscoveryItemsMapNewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryItemsMapNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryItemsMapNewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryLuxuryItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiDiscoveryGetDiscoveryLuxuryItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryLuxuryItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryLuxuryItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryLuxuryItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiDiscoveryGetDiscoveryLuxuryItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryLuxuryItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryLuxuryItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryRecentlyItemsUserPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiDiscoveryGetDiscoveryRecentlyItemsUserPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryRecentlyItemsUser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryRecentlyItemsUserPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiDiscoveryGetDiscoveryRecentlyItemsUserOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiDiscoveryGetDiscoveryRecentlyItemsUserOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/discovery/getDiscoveryRecentlyItemsUser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiDiscoveryGetDiscoveryRecentlyItemsUserOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiEmailSendRentalEmailLenderPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiEmailSendRentalEmailLenderPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/email/sendRentalEmailLender').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiEmailSendRentalEmailLenderPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiEmailSendRentalEmailLenderOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiEmailSendRentalEmailLenderOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/email/sendRentalEmailLender').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiEmailSendRentalEmailLenderOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiFeaturesValidatesApiVersionPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiFeaturesValidatesApiVersionPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/features/validatesApiVersion').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiFeaturesValidatesApiVersionPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiFeaturesValidatesApiVersionOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiFeaturesValidatesApiVersionOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/features/validatesApiVersion').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiFeaturesValidatesApiVersionOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCheckForItemUserReportedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCheckForItemUserReportedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/checkForItemUserReported').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCheckForItemUserReportedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCheckForItemUserReportedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCheckForItemUserReportedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/checkForItemUserReported').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCheckForItemUserReportedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemDeliveryPricesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateItemDeliveryPricesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemDeliveryPrices').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemDeliveryPricesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemDeliveryPricesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateItemDeliveryPricesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemDeliveryPrices').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemDeliveryPricesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemReportPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateItemReportPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemReport').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemReportPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemReportOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateItemReportOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemReport').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemReportOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionChargePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateItemRepositionChargePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionCharge').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionChargePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionChargeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateItemRepositionChargeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionCharge').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionChargeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionPaymentPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateItemRepositionPaymentPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionPayment').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionPaymentPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionPaymentOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateItemRepositionPaymentOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionPayment').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionPaymentOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionPaymentNewAmmountPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateItemRepositionPaymentNewAmmountPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionPaymentNewAmmount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionPaymentNewAmmountPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionPaymentNewAmmountOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateItemRepositionPaymentNewAmmountOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionPaymentNewAmmount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionPaymentNewAmmountOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionPaymentOriginalAmmountPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateItemRepositionPaymentOriginalAmmountPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionPaymentOriginalAmmount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionPaymentOriginalAmmountPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateItemRepositionPaymentOriginalAmmountOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateItemRepositionPaymentOriginalAmmountOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createItemRepositionPaymentOriginalAmmount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateItemRepositionPaymentOriginalAmmountOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRelItemAccesoriePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateRelItemAccesoriePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRelItemAccesorie').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRelItemAccesoriePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRelItemAccesorieOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateRelItemAccesorieOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRelItemAccesorie').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRelItemAccesorieOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRelUserTermWithItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateRelUserTermWithItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRelUserTermWithItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRelUserTermWithItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRelUserTermWithItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateRelUserTermWithItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRelUserTermWithItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRelUserTermWithItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRentalDisputePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateRentalDisputePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRentalDispute').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRentalDisputePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRentalDisputeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateRentalDisputeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRentalDispute').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRentalDisputeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemDelRetImagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateRntItemDelRetImagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemDelRetImage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemDelRetImagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemDelRetImageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateRntItemDelRetImageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemDelRetImage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemDelRetImageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemDelRetImageStagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateRntItemDelRetImageStagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemDelRetImageStage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemDelRetImageStagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemDelRetImageStageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateRntItemDelRetImageStageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemDelRetImageStage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemDelRetImageStageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemStatusPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateRntItemStatusPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemStatusPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemStatusOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateRntItemStatusOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemStatusOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemStatusCommentsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateRntItemStatusCommentsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemStatusComments').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemStatusCommentsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateRntItemStatusCommentsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateRntItemStatusCommentsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createRntItemStatusComments').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateRntItemStatusCommentsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUnavailableDatesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateUnavailableDatesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUnavailableDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUnavailableDatesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUnavailableDatesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateUnavailableDatesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUnavailableDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUnavailableDatesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateItemUserAdressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateUpdateItemUserAdressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateItemUserAdress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateItemUserAdressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateItemUserAdressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateUpdateItemUserAdressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateItemUserAdress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateItemUserAdressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateItemUserMapAdressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateUpdateItemUserMapAdressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateItemUserMapAdress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateItemUserMapAdressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateItemUserMapAdressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateUpdateItemUserMapAdressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateItemUserMapAdress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateItemUserMapAdressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateUpdateLndItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateUpdateLndItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemImagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateUpdateLndItemImagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItemImage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemImagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemImageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateUpdateLndItemImageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItemImage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemImageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemNewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateUpdateLndItemNewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItemNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemNewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemNewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateUpdateLndItemNewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItemNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemNewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemNewVerifyPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemCreateUpdateLndItemNewVerifyPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItemNewVerify').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemNewVerifyPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemCreateUpdateLndItemNewVerifyOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemCreateUpdateLndItemNewVerifyOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/createUpdateLndItemNewVerify').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemCreateUpdateLndItemNewVerifyOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemAccesoriesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemDeleteItemAccesoriesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemAccesories').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemAccesoriesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemAccesoriesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemDeleteItemAccesoriesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemAccesories').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemAccesoriesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemAddressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemDeleteItemAddressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemAddressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemAddressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemDeleteItemAddressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemAddressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemDeliveryPricesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemDeleteItemDeliveryPricesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemDeliveryPrices').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemDeliveryPricesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemDeliveryPricesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemDeleteItemDeliveryPricesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemDeliveryPrices').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemDeliveryPricesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemForSalePricePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemDeleteItemForSalePricePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemForSalePrice').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemForSalePricePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemForSalePriceOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemDeleteItemForSalePriceOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemForSalePrice').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemForSalePriceOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemPicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemDeleteItemPicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemPicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemPictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemDeleteItemPictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemPictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemTermsAndConditionsRelationToItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemDeleteItemTermsAndConditionsRelationToItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemTermsAndConditionsRelationToItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemTermsAndConditionsRelationToItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDeleteItemTermsAndConditionsRelationToItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemDeleteItemTermsAndConditionsRelationToItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/deleteItemTermsAndConditionsRelationToItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDeleteItemTermsAndConditionsRelationToItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDisableItemUserPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemDisableItemUserPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/disableItemUser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDisableItemUserPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemDisableItemUserOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemDisableItemUserOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/disableItemUser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemDisableItemUserOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemEnableDisableItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemEnableDisableItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/enableDisableItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemEnableDisableItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemEnableDisableItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemEnableDisableItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/enableDisableItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemEnableDisableItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetAllItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetAllItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getAllItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetAllItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetAllItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetAllItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getAllItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetAllItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetBlockingFeaturesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetBlockingFeaturesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getBlockingFeatures').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetBlockingFeaturesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetBlockingFeaturesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetBlockingFeaturesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getBlockingFeatures').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetBlockingFeaturesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetDiscoveryFavoriteItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetDiscoveryFavoriteItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getDiscoveryFavoriteItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetDiscoveryFavoriteItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetDiscoveryFavoriteItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetDiscoveryFavoriteItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getDiscoveryFavoriteItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetDiscoveryFavoriteItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetDiscoveryItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetDiscoveryItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getDiscoveryItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetDiscoveryItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetDiscoveryItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetDiscoveryItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getDiscoveryItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetDiscoveryItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetDiscoveryRecentlyItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetDiscoveryRecentlyItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getDiscoveryRecentlyItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetDiscoveryRecentlyItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetDiscoveryRecentlyItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetDiscoveryRecentlyItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getDiscoveryRecentlyItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetDiscoveryRecentlyItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCanDropOffPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemCanDropOffPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCanDropOff').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCanDropOffPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCanDropOffOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemCanDropOffOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCanDropOff').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCanDropOffOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCanPickUpPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemCanPickUpPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCanPickUp').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCanPickUpPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCanPickUpOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemCanPickUpOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCanPickUp').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCanPickUpOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCategoriesListPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemCategoriesListPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCategoriesList').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCategoriesListPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCategoriesListOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemCategoriesListOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCategoriesList').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCategoriesListOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCategoriesListNewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemCategoriesListNewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCategoriesListNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCategoriesListNewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemCategoriesListNewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemCategoriesListNewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemCategoriesListNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemCategoriesListNewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemDatePassedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemDatePassedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemDatePassed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemDatePassedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemDatePassedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemDatePassedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemDatePassed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemDatePassedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemImagesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemImagesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemImagesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemImagesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemImagesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemImagesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRelatedAccessoriesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemRelatedAccessoriesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRelatedAccessories').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRelatedAccessoriesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRelatedAccessoriesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemRelatedAccessoriesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRelatedAccessories').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRelatedAccessoriesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRepositionChargePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemRepositionChargePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRepositionCharge').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRepositionChargePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRepositionChargeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemRepositionChargeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRepositionCharge').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRepositionChargeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRepositionChargeDeclinedRenterPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemRepositionChargeDeclinedRenterPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRepositionChargeDeclinedRenter').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRepositionChargeDeclinedRenterPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRepositionChargeDeclinedRenterOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemRepositionChargeDeclinedRenterOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRepositionChargeDeclinedRenter').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRepositionChargeDeclinedRenterOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRepositionChargeFinalCallPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemRepositionChargeFinalCallPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRepositionChargeFinalCall').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRepositionChargeFinalCallPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRepositionChargeFinalCallOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemRepositionChargeFinalCallOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRepositionChargeFinalCall').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRepositionChargeFinalCallOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRevsAndStatsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemRevsAndStatsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRevsAndStats').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRevsAndStatsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemRevsAndStatsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemRevsAndStatsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemRevsAndStats').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemRevsAndStatsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemUnavailableDatesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemUnavailableDatesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemUnavailableDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemUnavailableDatesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemUnavailableDatesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemUnavailableDatesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemUnavailableDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemUnavailableDatesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemUserAdressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemUserAdressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemUserAdress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemUserAdressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemUserAdressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemUserAdressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemUserAdress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemUserAdressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemUserUnavailableDatesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemUserUnavailableDatesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemUserUnavailableDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemUserUnavailableDatesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemUserUnavailableDatesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemUserUnavailableDatesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemUserUnavailableDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemUserUnavailableDatesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemsRentalHistoryPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemsRentalHistoryPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemsRentalHistory').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemsRentalHistoryPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemsRentalHistoryOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemsRentalHistoryOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemsRentalHistory').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemsRentalHistoryOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemsRentalHistoryNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetItemsRentalHistoryNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemsRentalHistoryNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemsRentalHistoryNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetItemsRentalHistoryNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetItemsRentalHistoryNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getItemsRentalHistoryNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetItemsRentalHistoryNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetLastFiveCreatedItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetLastFiveCreatedItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getLastFiveCreatedItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetLastFiveCreatedItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetLastFiveCreatedItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetLastFiveCreatedItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getLastFiveCreatedItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetLastFiveCreatedItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetLastTenCreatedItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetLastTenCreatedItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getLastTenCreatedItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetLastTenCreatedItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetLastTenCreatedItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetLastTenCreatedItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getLastTenCreatedItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetLastTenCreatedItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetLenderItemsLocationPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetLenderItemsLocationPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getLenderItemsLocation').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetLenderItemsLocationPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetLenderItemsLocationOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetLenderItemsLocationOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getLenderItemsLocation').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetLenderItemsLocationOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetMyFavoriteItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetMyFavoriteItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getMyFavoriteItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetMyFavoriteItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetMyFavoriteItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetMyFavoriteItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getMyFavoriteItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetMyFavoriteItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetMyFavoriteItemsWithCondCatPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetMyFavoriteItemsWithCondCatPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getMyFavoriteItemsWithCondCat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetMyFavoriteItemsWithCondCatPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetMyFavoriteItemsWithCondCatOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetMyFavoriteItemsWithCondCatOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getMyFavoriteItemsWithCondCat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetMyFavoriteItemsWithCondCatOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetNearMeItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetNearMeItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getNearMeItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetNearMeItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetNearMeItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetNearMeItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getNearMeItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetNearMeItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRecentlyVieweddItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRecentlyVieweddItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRecentlyVieweddItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRecentlyVieweddItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRecentlyVieweddItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRecentlyVieweddItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRecentlyVieweddItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRecentlyVieweddItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRentalIdByCodePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRentalIdByCodePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRentalIdByCode').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRentalIdByCodePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRentalIdByCodeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRentalIdByCodeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRentalIdByCode').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRentalIdByCodeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRentedItemAddressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRentedItemAddressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRentedItemAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRentedItemAddressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRentedItemAddressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRentedItemAddressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRentedItemAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRentedItemAddressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetReportReasonsListPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetReportReasonsListPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getReportReasonsList').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetReportReasonsListPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetReportReasonsListOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetReportReasonsListOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getReportReasonsList').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetReportReasonsListOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemImagesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemImagesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemImagesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemImagesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemImagesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemImagesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemPaymentMethodPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemPaymentMethodPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemPaymentMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemPaymentMethodPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemPaymentMethodOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemPaymentMethodOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemPaymentMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemPaymentMethodOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemReviewsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemReviewsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemReviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemReviewsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemReviewsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemReviewsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemReviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemReviewsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemStageImagesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemStageImagesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemStageImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemStageImagesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemStageImagesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemStageImagesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemStageImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemStageImagesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemStatusImagesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemStatusImagesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemStatusImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemStatusImagesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemStatusImagesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemStatusImagesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemStatusImages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemStatusImagesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemTimeLineStatusPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemTimeLineStatusPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemTimeLineStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemTimeLineStatusPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemTimeLineStatusOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemTimeLineStatusOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemTimeLineStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemTimeLineStatusOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemTimeLineStatusErrorPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemTimeLineStatusErrorPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemTimeLineStatusError').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemTimeLineStatusErrorPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemTimeLineStatusErrorOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemTimeLineStatusErrorOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemTimeLineStatusError').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemTimeLineStatusErrorOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemUserReviewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemUserReviewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemUserReview').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemUserReviewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemUserReviewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemUserReviewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemUserReview').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemUserReviewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemUserReviewNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetRntItemUserReviewNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemUserReviewNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemUserReviewNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetRntItemUserReviewNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetRntItemUserReviewNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getRntItemUserReviewNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetRntItemUserReviewNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetSearchForItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetSearchForItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getSearchForItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetSearchForItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetSearchForItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetSearchForItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getSearchForItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetSearchForItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetSearchForItemsAllTestPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetSearchForItemsAllTestPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getSearchForItemsAllTest').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetSearchForItemsAllTestPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetSearchForItemsAllTestOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetSearchForItemsAllTestOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getSearchForItemsAllTest').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetSearchForItemsAllTestOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetSearchForItemsByIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetSearchForItemsByIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getSearchForItemsById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetSearchForItemsByIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetSearchForItemsByIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetSearchForItemsByIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getSearchForItemsById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetSearchForItemsByIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserDefaultAddressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetUserDefaultAddressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserDefaultAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserDefaultAddressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserDefaultAddressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetUserDefaultAddressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserDefaultAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserDefaultAddressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserLndItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetUserLndItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserLndItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserLndItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserLndItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetUserLndItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserLndItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserLndItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserLndItemsProfilePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetUserLndItemsProfilePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserLndItemsProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserLndItemsProfilePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserLndItemsProfileOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetUserLndItemsProfileOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserLndItemsProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserLndItemsProfileOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserLndItemsProfileNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetUserLndItemsProfileNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserLndItemsProfileNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserLndItemsProfileNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserLndItemsProfileNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetUserLndItemsProfileNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserLndItemsProfileNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserLndItemsProfileNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserRntItemByIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetUserRntItemByIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserRntItemById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserRntItemByIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserRntItemByIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetUserRntItemByIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserRntItemById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserRntItemByIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserRntItemsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemGetUserRntItemsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserRntItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserRntItemsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemGetUserRntItemsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemGetUserRntItemsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/getUserRntItems').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemGetUserRntItemsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemInvalidateRentedItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemInvalidateRentedItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/invalidateRentedItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemInvalidateRentedItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemInvalidateRentedItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemInvalidateRentedItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/invalidateRentedItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemInvalidateRentedItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemIsOwnItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemIsOwnItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/isOwnItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemIsOwnItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemIsOwnItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemIsOwnItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/isOwnItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemIsOwnItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemMainItemSearchPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearch').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemMainItemSearchOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearch').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchByIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemMainItemSearchByIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearchById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchByIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchByIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemMainItemSearchByIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearchById').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchByIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchByIdNewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemMainItemSearchByIdNewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearchByIdNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchByIdNewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchByIdNewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemMainItemSearchByIdNewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearchByIdNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchByIdNewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemMainItemSearchNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearchNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemMainItemSearchNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemMainItemSearchNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/mainItemSearchNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemMainItemSearchNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetDondeStatusRntItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemSetDondeStatusRntItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setDondeStatusRntItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetDondeStatusRntItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetDondeStatusRntItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemSetDondeStatusRntItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setDondeStatusRntItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetDondeStatusRntItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetRntItemActiveStatusPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemSetRntItemActiveStatusPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setRntItemActiveStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetRntItemActiveStatusPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetRntItemActiveStatusOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemSetRntItemActiveStatusOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setRntItemActiveStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetRntItemActiveStatusOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemSetSaveRentedItemPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemSetSaveRentedItemOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItem').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemSetSaveRentedItemNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItemNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemSetSaveRentedItemNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItemNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemNewFlowVerifyPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemSetSaveRentedItemNewFlowVerifyPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItemNewFlowVerify').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemNewFlowVerifyPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemNewFlowVerifyOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemSetSaveRentedItemNewFlowVerifyOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItemNewFlowVerify').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemNewFlowVerifyOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemNewSchemaPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemSetSaveRentedItemNewSchemaPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItemNewSchema').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemNewSchemaPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemSetSaveRentedItemNewSchemaOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemSetSaveRentedItemNewSchemaOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/setSaveRentedItemNewSchema').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemSetSaveRentedItemNewSchemaOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUpdateItemAcceptOffersPriceSalePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemUpdateItemAcceptOffersPriceSalePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/updateItemAcceptOffersPriceSale').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUpdateItemAcceptOffersPriceSalePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUpdateItemAcceptOffersPriceSaleOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemUpdateItemAcceptOffersPriceSaleOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/updateItemAcceptOffersPriceSale').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUpdateItemAcceptOffersPriceSaleOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUpdateItemAddressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemUpdateItemAddressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/updateItemAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUpdateItemAddressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUpdateItemAddressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemUpdateItemAddressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/updateItemAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUpdateItemAddressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUpdateItemRepositionPaymentToDisputedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemUpdateItemRepositionPaymentToDisputedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/updateItemRepositionPaymentToDisputed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUpdateItemRepositionPaymentToDisputedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUpdateItemRepositionPaymentToDisputedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemUpdateItemRepositionPaymentToDisputedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/updateItemRepositionPaymentToDisputed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUpdateItemRepositionPaymentToDisputedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUploadDeliveryPicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemUploadDeliveryPicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/uploadDeliveryPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUploadDeliveryPicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUploadDeliveryPictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemUploadDeliveryPictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/uploadDeliveryPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUploadDeliveryPictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUploadItemPicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemUploadItemPicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/uploadItemPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUploadItemPicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUploadItemPictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemUploadItemPictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/uploadItemPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUploadItemPictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUploadReturnalPicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemUploadReturnalPicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/uploadReturnalPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUploadReturnalPicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemUploadReturnalPictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemUploadReturnalPictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/uploadReturnalPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemUploadReturnalPictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesCanCancelRentalPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemValidatesCanCancelRentalPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesCanCancelRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesCanCancelRentalPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesCanCancelRentalOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemValidatesCanCancelRentalOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesCanCancelRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesCanCancelRentalOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesCanReviewRentalPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemValidatesCanReviewRentalPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesCanReviewRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesCanReviewRentalPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesCanReviewRentalOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemValidatesCanReviewRentalOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesCanReviewRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesCanReviewRentalOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesItemRentalDatesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemValidatesItemRentalDatesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesItemRentalDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesItemRentalDatesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesItemRentalDatesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemValidatesItemRentalDatesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesItemRentalDates').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesItemRentalDatesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesItemRentalDatesServerPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemValidatesItemRentalDatesServerPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesItemRentalDatesServer').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesItemRentalDatesServerPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesItemRentalDatesServerOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemValidatesItemRentalDatesServerOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesItemRentalDatesServer').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesItemRentalDatesServerOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesItemRepositionPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemValidatesItemRepositionPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesItemReposition').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesItemRepositionPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesItemRepositionOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemValidatesItemRepositionOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesItemReposition').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesItemRepositionOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesRentalStartsTodayPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiItemValidatesRentalStartsTodayPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesRentalStartsToday').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesRentalStartsTodayPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiItemValidatesRentalStartsTodayOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiItemValidatesRentalStartsTodayOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/item/validatesRentalStartsToday').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiItemValidatesRentalStartsTodayOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiLocalDisabledeleteuserPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var apiLocalDisabledeleteuserPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/local/disabledeleteuser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiLocalDisabledeleteuserPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiLocalDisabledeleteuserOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var apiLocalDisabledeleteuserOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/local/disabledeleteuser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiLocalDisabledeleteuserOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageCreateUserConversationPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversation').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageCreateUserConversationOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversation').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationMessagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageCreateUserConversationMessagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversationMessage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationMessagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationMessageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageCreateUserConversationMessageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversationMessage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationMessageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationMessageNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageCreateUserConversationMessageNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversationMessageNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationMessageNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationMessageNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageCreateUserConversationMessageNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversationMessageNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationMessageNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageCreateUserConversationNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversationNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageCreateUserConversationNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageCreateUserConversationNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/createUserConversationNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageCreateUserConversationNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetConversationMessagesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageGetConversationMessagesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getConversationMessages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetConversationMessagesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetConversationMessagesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageGetConversationMessagesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getConversationMessages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetConversationMessagesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetUserConversationTotalUnreadMessagesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageGetUserConversationTotalUnreadMessagesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getUserConversationTotalUnreadMessages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetUserConversationTotalUnreadMessagesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetUserConversationTotalUnreadMessagesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageGetUserConversationTotalUnreadMessagesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getUserConversationTotalUnreadMessages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetUserConversationTotalUnreadMessagesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetUserConversationsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageGetUserConversationsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getUserConversations').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetUserConversationsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetUserConversationsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageGetUserConversationsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getUserConversations').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetUserConversationsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetUserConversationsNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageGetUserConversationsNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getUserConversationsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetUserConversationsNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageGetUserConversationsNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageGetUserConversationsNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/getUserConversationsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageGetUserConversationsNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageUpateConversationMessageStatusPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiMessageUpateConversationMessageStatusPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/upateConversationMessageStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageUpateConversationMessageStatusPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiMessageUpateConversationMessageStatusOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiMessageUpateConversationMessageStatusOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/message/upateConversationMessageStatus').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiMessageUpateConversationMessageStatusOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentCreatePaymentMethodFromCameraPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentCreatePaymentMethodFromCameraPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/createPaymentMethodFromCamera').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentCreatePaymentMethodFromCameraPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentCreatePaymentMethodFromCameraOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentCreatePaymentMethodFromCameraOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/createPaymentMethodFromCamera').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentCreatePaymentMethodFromCameraOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentCreateRentalPaymentNoDepositPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentCreateRentalPaymentNoDepositPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/createRentalPaymentNoDeposit').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentCreateRentalPaymentNoDepositPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentCreateRentalPaymentNoDepositOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentCreateRentalPaymentNoDepositOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/createRentalPaymentNoDeposit').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentCreateRentalPaymentNoDepositOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentCreateRentalPaymentWithFeePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentCreateRentalPaymentWithFeePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/createRentalPaymentWithFee').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentCreateRentalPaymentWithFeePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentCreateRentalPaymentWithFeeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentCreateRentalPaymentWithFeeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/createRentalPaymentWithFee').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentCreateRentalPaymentWithFeeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentDeletePaymentMethodPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentDeletePaymentMethodPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/deletePaymentMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentDeletePaymentMethodPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentDeletePaymentMethodOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentDeletePaymentMethodOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/deletePaymentMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentDeletePaymentMethodOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentGetUserPaymentMethodsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentGetUserPaymentMethodsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/getUserPaymentMethods').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentGetUserPaymentMethodsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentGetUserPaymentMethodsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentGetUserPaymentMethodsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/getUserPaymentMethods').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentGetUserPaymentMethodsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentSavePaymentMethodPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentSavePaymentMethodPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/savePaymentMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentSavePaymentMethodPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentSavePaymentMethodOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentSavePaymentMethodOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/savePaymentMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentSavePaymentMethodOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentSetUserCustomerDefaultPaymenyMethodPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentSetUserCustomerDefaultPaymenyMethodPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/setUserCustomerDefaultPaymenyMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentSetUserCustomerDefaultPaymenyMethodPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentSetUserCustomerDefaultPaymenyMethodOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentSetUserCustomerDefaultPaymenyMethodOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/setUserCustomerDefaultPaymenyMethod').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentSetUserCustomerDefaultPaymenyMethodOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentSettleRentalPaymentPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentSettleRentalPaymentPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/settleRentalPayment').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentSettleRentalPaymentPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentSettleRentalPaymentOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentSettleRentalPaymentOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/settleRentalPayment').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentSettleRentalPaymentOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentValidatesApplePaymentPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPaymentValidatesApplePaymentPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/validatesApplePayment').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentValidatesApplePaymentPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPaymentValidatesApplePaymentOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPaymentValidatesApplePaymentOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/payment/validatesApplePayment').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPaymentValidatesApplePaymentOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPictureItempicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var apiPictureItempicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/picture/itempicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPictureItempicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPictureProfilepicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var apiPictureProfilepicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/picture/profilepicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPictureProfilepicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPictureRentalpicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var apiPictureRentalpicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/picture/rentalpicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPictureRentalpicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationCreateUserOneSignalDeviceIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationCreateUserOneSignalDeviceIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/createUserOneSignalDeviceId').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationCreateUserOneSignalDeviceIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationCreateUserOneSignalDeviceIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationCreateUserOneSignalDeviceIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/createUserOneSignalDeviceId').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationCreateUserOneSignalDeviceIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationGetUserNonReadNotificationsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationGetUserNonReadNotificationsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/getUserNonReadNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationGetUserNonReadNotificationsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationGetUserNonReadNotificationsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationGetUserNonReadNotificationsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/getUserNonReadNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationGetUserNonReadNotificationsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationGetUserNotificationsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationGetUserNotificationsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/getUserNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationGetUserNotificationsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationGetUserNotificationsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationGetUserNotificationsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/getUserNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationGetUserNotificationsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationGetUserOneSignalDeviceIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationGetUserOneSignalDeviceIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/getUserOneSignalDeviceId').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationGetUserOneSignalDeviceIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationGetUserOneSignalDeviceIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationGetUserOneSignalDeviceIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/getUserOneSignalDeviceId').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationGetUserOneSignalDeviceIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendNotificationGeneralNotificationPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendNotificationGeneralNotificationPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendNotificationGeneralNotification').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendNotificationGeneralNotificationPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendNotificationGeneralNotificationOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendNotificationGeneralNotificationOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendNotificationGeneralNotification').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendNotificationGeneralNotificationOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifIdVerificationPendingAndCompletedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifIdVerificationPendingAndCompletedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifIdVerificationPendingAndCompleted').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifIdVerificationPendingAndCompletedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifIdVerificationPendingAndCompletedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifIdVerificationPendingAndCompletedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifIdVerificationPendingAndCompleted').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifIdVerificationPendingAndCompletedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifInboxMessagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifInboxMessagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifInboxMessage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifInboxMessagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifInboxMessageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifInboxMessageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifInboxMessage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifInboxMessageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifMemberInvitedCommPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifMemberInvitedCommPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifMemberInvitedComm').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifMemberInvitedCommPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifMemberInvitedCommOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifMemberInvitedCommOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifMemberInvitedComm').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifMemberInvitedCommOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifNoStripeAccountRentPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifNoStripeAccountRentPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifNoStripeAccountRent').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifNoStripeAccountRentPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifNoStripeAccountRentOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifNoStripeAccountRentOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifNoStripeAccountRent').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifNoStripeAccountRentOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcess').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcess').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRental').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSections').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSections').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSectionsNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSectionsNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSectionsNoPush').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSectionsNoPush').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDayPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDayPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDay').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDayPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDayOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDayOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/sendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDay').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSendPushNotifRentalProcessRentalNewFlowSectionsNoPushSameDayOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSetUserOpenedNotificationsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationSetUserOpenedNotificationsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/setUserOpenedNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSetUserOpenedNotificationsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationSetUserOpenedNotificationsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationSetUserOpenedNotificationsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/setUserOpenedNotifications').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationSetUserOpenedNotificationsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationUpdateUserOneSignalDeviceIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiPushnotificationUpdateUserOneSignalDeviceIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/updateUserOneSignalDeviceId').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationUpdateUserOneSignalDeviceIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiPushnotificationUpdateUserOneSignalDeviceIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiPushnotificationUpdateUserOneSignalDeviceIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/pushnotification/updateUserOneSignalDeviceId').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiPushnotificationUpdateUserOneSignalDeviceIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateDetReviewThreadPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateDetReviewThreadPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createDetReviewThread').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateDetReviewThreadPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateDetReviewThreadOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateDetReviewThreadOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createDetReviewThread').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateDetReviewThreadOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateRntItemUserReviewExperiencePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUpdateRntItemUserReviewExperiencePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateRntItemUserReviewExperience').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateRntItemUserReviewExperiencePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateRntItemUserReviewExperienceOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUpdateRntItemUserReviewExperienceOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateRntItemUserReviewExperience').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateRntItemUserReviewExperienceOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfilePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUpdateUserProfilePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfilePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUpdateUserProfileOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileAllPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUpdateUserProfileAllPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileAll').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileAllPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileAllOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUpdateUserProfileAllOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileAll').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileAllOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileAllNickNameDatePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUpdateUserProfileAllNickNameDatePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileAllNickNameDate').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileAllNickNameDatePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileAllNickNameDateOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUpdateUserProfileAllNickNameDateOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileAllNickNameDate').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileAllNickNameDateOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileDescriptionPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUpdateUserProfileDescriptionPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileDescription').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileDescriptionPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileDescriptionOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUpdateUserProfileDescriptionOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileDescription').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileDescriptionOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileNewPagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUpdateUserProfileNewPagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileNewPage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileNewPagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserProfileNewPageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUpdateUserProfileNewPageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserProfileNewPage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserProfileNewPageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserTermsAndConditionsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUpdateUserTermsAndConditionsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserTermsAndConditions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserTermsAndConditionsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUpdateUserTermsAndConditionsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUpdateUserTermsAndConditionsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUpdateUserTermsAndConditions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUpdateUserTermsAndConditionsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUser').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserFavoritePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserFavoritePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserFavorite').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserFavoritePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserFavoriteOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserFavoriteOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserFavorite').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserFavoriteOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserNewFlowWithApplePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserNewFlowWithApplePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserNewFlowWithApple').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserNewFlowWithApplePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserNewFlowWithAppleOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserNewFlowWithAppleOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserNewFlowWithApple').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserNewFlowWithAppleOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserReportPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserReportPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserReport').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserReportPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserReportOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserReportOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserReport').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserReportOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserReviewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserReviewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserReview').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserReviewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserReviewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserReviewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserReview').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserReviewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserTermsAndConditionsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserTermsAndConditionsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserTermsAndConditions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserTermsAndConditionsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserTermsAndConditionsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserTermsAndConditionsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserTermsAndConditions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserTermsAndConditionsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserViewedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserCreateUserViewedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserViewed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserViewedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserCreateUserViewedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserCreateUserViewedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/createUserViewed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserCreateUserViewedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetGivenReviewsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetGivenReviewsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getGivenReviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetGivenReviewsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetGivenReviewsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetGivenReviewsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getGivenReviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetGivenReviewsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetGivenReviewsFixedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetGivenReviewsFixedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getGivenReviewsFixed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetGivenReviewsFixedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetGivenReviewsFixedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetGivenReviewsFixedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getGivenReviewsFixed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetGivenReviewsFixedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetGivenReviewsNewFormatPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetGivenReviewsNewFormatPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getGivenReviewsNewFormat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetGivenReviewsNewFormatPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetGivenReviewsNewFormatOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetGivenReviewsNewFormatOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getGivenReviewsNewFormat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetGivenReviewsNewFormatOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetLenderProfileInfoPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetLenderProfileInfoPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getLenderProfileInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetLenderProfileInfoPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetLenderProfileInfoOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetLenderProfileInfoOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getLenderProfileInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetLenderProfileInfoOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetLenderProfileInfoNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetLenderProfileInfoNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getLenderProfileInfoNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetLenderProfileInfoNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetLenderProfileInfoNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetLenderProfileInfoNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getLenderProfileInfoNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetLenderProfileInfoNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetLendersNearMePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetLendersNearMePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getLendersNearMe').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetLendersNearMePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetLendersNearMeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetLendersNearMeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getLendersNearMe').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetLendersNearMeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetReceivedReviewsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetReceivedReviewsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getReceivedReviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetReceivedReviewsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetReceivedReviewsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetReceivedReviewsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getReceivedReviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetReceivedReviewsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetReceivedReviewsFixedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetReceivedReviewsFixedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getReceivedReviewsFixed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetReceivedReviewsFixedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetReceivedReviewsFixedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetReceivedReviewsFixedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getReceivedReviewsFixed').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetReceivedReviewsFixedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetReceivedReviewsNewFormatPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetReceivedReviewsNewFormatPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getReceivedReviewsNewFormat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetReceivedReviewsNewFormatPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetReceivedReviewsNewFormatOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetReceivedReviewsNewFormatOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getReceivedReviewsNewFormat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetReceivedReviewsNewFormatOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserBasedTokenPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserBasedTokenPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserBasedToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserBasedTokenPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserBasedTokenOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserBasedTokenOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserBasedToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserBasedTokenOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserByEmailPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserByEmailPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserByEmail').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserByEmailPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserByEmailOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserByEmailOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserByEmail').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserByEmailOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserCanViewFeaturedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserCanViewFeaturedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserCanViewFeatured').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserCanViewFeaturedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserCanViewFeaturedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserCanViewFeaturedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserCanViewFeatured').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserCanViewFeaturedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserCanViewLuxuryPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserCanViewLuxuryPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserCanViewLuxury').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserCanViewLuxuryPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserCanViewLuxuryOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserCanViewLuxuryOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserCanViewLuxury').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserCanViewLuxuryOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserDevicesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserDevicesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserDevices').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserDevicesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserDevicesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserDevicesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserDevices').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserDevicesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserDevicesAllPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserDevicesAllPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserDevicesAll').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserDevicesAllPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserDevicesAllOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserDevicesAllOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserDevicesAll').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserDevicesAllOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserIsVerifiedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserIsVerifiedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserIsVerified').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserIsVerifiedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserIsVerifiedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserIsVerifiedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserIsVerified').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserIsVerifiedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserLndItemsNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserLndItemsNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserLndItemsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserLndItemsNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserLndItemsNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserLndItemsNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserLndItemsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserLndItemsNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserLndItemsNewFlowPublicProfilePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserLndItemsNewFlowPublicProfilePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserLndItemsNewFlowPublicProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserLndItemsNewFlowPublicProfilePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserLndItemsNewFlowPublicProfileOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserLndItemsNewFlowPublicProfileOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserLndItemsNewFlowPublicProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserLndItemsNewFlowPublicProfileOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserOauthTokenPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserOauthTokenOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserOauthTokenNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthTokenNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserOauthTokenNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthTokenNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenNewFlowWithApplePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserOauthTokenNewFlowWithApplePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthTokenNewFlowWithApple').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenNewFlowWithApplePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenNewFlowWithAppleOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserOauthTokenNewFlowWithAppleOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthTokenNewFlowWithApple').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenNewFlowWithAppleOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenUserCPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserOauthTokenUserCPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthTokenUserC').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenUserCPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserOauthTokenUserCOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserOauthTokenUserCOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserOauthTokenUserC').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserOauthTokenUserCOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfileInfoPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfileInfoOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfileInfoNewPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfileInfoNewOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNew').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewFlowOwnPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfileInfoNewFlowOwnPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNewFlowOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewFlowOwnPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewFlowOwnOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfileInfoNewFlowOwnOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNewFlowOwn').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewFlowOwnOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewPagePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfileInfoNewPagePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNewPage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewPagePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewPageOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfileInfoNewPageOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNewPage').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewPageOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewReviewFormatPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfileInfoNewReviewFormatPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNewReviewFormat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewReviewFormatPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileInfoNewReviewFormatOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfileInfoNewReviewFormatOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileInfoNewReviewFormat').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileInfoNewReviewFormatOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfilePicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfilePicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfilePicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfilePicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfilePictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfilePictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfilePicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfilePictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileStatsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfileStatsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileStats').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileStatsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileStatsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfileStatsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileStats').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileStatsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileStatsSettingsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserProfileStatsSettingsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileStatsSettings').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileStatsSettingsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserProfileStatsSettingsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserProfileStatsSettingsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserProfileStatsSettings').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserProfileStatsSettingsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserRntItemsNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserRntItemsNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserRntItemsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserRntItemsNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserRntItemsNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserRntItemsNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserRntItemsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserRntItemsNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserSetupInfoPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserSetupInfoPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserSetupInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserSetupInfoPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserSetupInfoOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserSetupInfoOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserSetupInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserSetupInfoOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserTermsAndConditionsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserGetUserTermsAndConditionsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserTermsAndConditions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserTermsAndConditionsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserGetUserTermsAndConditionsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserGetUserTermsAndConditionsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/getUserTermsAndConditions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserGetUserTermsAndConditionsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateRepositionChargeNotChargePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateRepositionChargeNotChargePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateRepositionChargeNotCharge').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateRepositionChargeNotChargePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateRepositionChargeNotChargeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateRepositionChargeNotChargeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateRepositionChargeNotCharge').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateRepositionChargeNotChargeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserDriverLicenceIdPicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateUserDriverLicenceIdPicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserDriverLicenceIdPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserDriverLicenceIdPicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserDriverLicenceIdPictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateUserDriverLicenceIdPictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserDriverLicenceIdPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserDriverLicenceIdPictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserNameAndDescriptionPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateUserNameAndDescriptionPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserNameAndDescription').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserNameAndDescriptionPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserNameAndDescriptionOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateUserNameAndDescriptionOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserNameAndDescription').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserNameAndDescriptionOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfilePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateUserProfilePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfilePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfileOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateUserProfileOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfile').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfileOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfileDefaultAddressPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateUserProfileDefaultAddressPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfileDefaultAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfileDefaultAddressPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfileDefaultAddressOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateUserProfileDefaultAddressOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfileDefaultAddress').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfileDefaultAddressOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfileNamePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateUserProfileNamePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfileName').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfileNamePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfileNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateUserProfileNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfileName').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfileNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfileNameWithUsernamePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateUserProfileNameWithUsernamePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfileNameWithUsername').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfileNameWithUsernamePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfileNameWithUsernameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateUserProfileNameWithUsernameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfileNameWithUsername').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfileNameWithUsernameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfilePicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUpdateUserProfilePicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfilePicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfilePicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUpdateUserProfilePictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUpdateUserProfilePictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/updateUserProfilePicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUpdateUserProfilePictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUploadDriverLicenceIdPicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUploadDriverLicenceIdPicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/uploadDriverLicenceIdPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUploadDriverLicenceIdPicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUploadDriverLicenceIdPictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUploadDriverLicenceIdPictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/uploadDriverLicenceIdPicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUploadDriverLicenceIdPictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUploadProfilePicturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserUploadProfilePicturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/uploadProfilePicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUploadProfilePicturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserUploadProfilePictureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserUploadProfilePictureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/uploadProfilePicture').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserUploadProfilePictureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesExistingEmailAccountPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesExistingEmailAccountPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesExistingEmailAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesExistingEmailAccountPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesExistingEmailAccountOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesExistingEmailAccountOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesExistingEmailAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesExistingEmailAccountOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesExistingEmailAccountNickNamePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesExistingEmailAccountNickNamePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesExistingEmailAccountNickName').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesExistingEmailAccountNickNamePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesExistingEmailAccountNickNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesExistingEmailAccountNickNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesExistingEmailAccountNickName').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesExistingEmailAccountNickNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesExistingNickNamePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesExistingNickNamePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesExistingNickName').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesExistingNickNamePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesExistingNickNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesExistingNickNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesExistingNickName').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesExistingNickNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesPhoneNumberPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesPhoneNumberPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesPhoneNumber').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesPhoneNumberPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesPhoneNumberOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesPhoneNumberOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesPhoneNumber').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesPhoneNumberOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesPhoneNumberCancelPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesPhoneNumberCancelPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesPhoneNumberCancel').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesPhoneNumberCancelPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesPhoneNumberCancelOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesPhoneNumberCancelOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesPhoneNumberCancel').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesPhoneNumberCancelOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesPhoneNumberCodePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesPhoneNumberCodePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesPhoneNumberCode').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesPhoneNumberCodePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesPhoneNumberCodeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesPhoneNumberCodeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesPhoneNumberCode').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesPhoneNumberCodeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserExitsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesUserExitsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserExits').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserExitsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserExitsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesUserExitsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserExits').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserExitsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserExitsNewFlowPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesUserExitsNewFlowPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserExitsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserExitsNewFlowPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserExitsNewFlowOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesUserExitsNewFlowOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserExitsNewFlow').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserExitsNewFlowOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserExitsNewFlowWithApplePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesUserExitsNewFlowWithApplePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserExitsNewFlowWithApple').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserExitsNewFlowWithApplePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserExitsNewFlowWithAppleOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesUserExitsNewFlowWithAppleOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserExitsNewFlowWithApple').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserExitsNewFlowWithAppleOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserPendingInfoPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesUserPendingInfoPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserPendingInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserPendingInfoPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserPendingInfoOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesUserPendingInfoOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserPendingInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserPendingInfoOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserTokenPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserValidatesUserTokenPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserTokenPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserValidatesUserTokenOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserValidatesUserTokenOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/validatesUserToken').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserValidatesUserTokenOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserVerificationIdUserShuftiPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserVerificationIdUserShuftiPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/verificationIdUserShufti').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserVerificationIdUserShuftiPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserVerificationIdUserShuftiOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserVerificationIdUserShuftiOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/verificationIdUserShufti').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserVerificationIdUserShuftiOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserVerificationIdUserStripePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserVerificationIdUserStripePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/verificationIdUserStripe').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserVerificationIdUserStripePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserVerificationIdUserStripeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserVerificationIdUserStripeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/user/verificationIdUserStripe').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserVerificationIdUserStripeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutAttachExistingStripeAccountPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutAttachExistingStripeAccountPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/attachExistingStripeAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutAttachExistingStripeAccountPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutAttachExistingStripeAccountOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutAttachExistingStripeAccountOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/attachExistingStripeAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutAttachExistingStripeAccountOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserPayOutAccountPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutGetUserPayOutAccountPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserPayOutAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserPayOutAccountPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserPayOutAccountOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutGetUserPayOutAccountOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserPayOutAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserPayOutAccountOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserPayOutAccountValidatePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutGetUserPayOutAccountValidatePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserPayOutAccountValidate').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserPayOutAccountValidatePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserPayOutAccountValidateOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutGetUserPayOutAccountValidateOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserPayOutAccountValidate').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserPayOutAccountValidateOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserStripeAcctPendingInfoPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutGetUserStripeAcctPendingInfoPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserStripeAcctPendingInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserStripeAcctPendingInfoPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserStripeAcctPendingInfoOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutGetUserStripeAcctPendingInfoOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserStripeAcctPendingInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserStripeAcctPendingInfoOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserStripeAcctPendingInfoListPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutGetUserStripeAcctPendingInfoListPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserStripeAcctPendingInfoList').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserStripeAcctPendingInfoListPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutGetUserStripeAcctPendingInfoListOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutGetUserStripeAcctPendingInfoListOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/getUserStripeAcctPendingInfoList').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutGetUserStripeAcctPendingInfoListOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateIndAccountMissingInfoPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutUpdateIndAccountMissingInfoPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateIndAccountMissingInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateIndAccountMissingInfoPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateIndAccountMissingInfoOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutUpdateIndAccountMissingInfoOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateIndAccountMissingInfo').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateIndAccountMissingInfoOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateUserPayOutAccountPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutUpdateUserPayOutAccountPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateUserPayOutAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateUserPayOutAccountPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateUserPayOutAccountOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutUpdateUserPayOutAccountOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateUserPayOutAccount').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateUserPayOutAccountOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateUserPayOutAccountCustomPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutUpdateUserPayOutAccountCustomPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateUserPayOutAccountCustom').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateUserPayOutAccountCustomPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateUserPayOutAccountCustomOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutUpdateUserPayOutAccountCustomOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateUserPayOutAccountCustom').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateUserPayOutAccountCustomOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateUserPayOutAccountCustomWithPersonPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var apiUserpayoutUpdateUserPayOutAccountCustomWithPersonPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateUserPayOutAccountCustomWithPerson').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateUserPayOutAccountCustomWithPersonPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.apiUserpayoutUpdateUserPayOutAccountCustomWithPersonOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods'], ['body']);
        
        var apiUserpayoutUpdateUserPayOutAccountCustomWithPersonOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/api/userpayout/updateUserPayOutAccountCustomWithPerson').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, ['Access-Control-Allow-Origin', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Methods']),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(apiUserpayoutUpdateUserPayOutAccountCustomWithPersonOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.stripeWebhookPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var stripeWebhookPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/stripe/webhook').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(stripeWebhookPostRequest, authType, additionalParams, config.apiKey);
    };
    

    return apigClient;
};
