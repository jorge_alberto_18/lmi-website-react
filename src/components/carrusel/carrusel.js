/* eslint-disable no-undef */

import React, { useState, useEffect, useRef } from "react";
import Swiper from 'react-id-swiper';
import 'swiper/swiper-bundle.css'
import "./carrusel.scss";
import { withRouter } from 'react-router-dom';
import {Redirect} from 'react-router-dom';

import market from '../../img/slider-items/market.svg';

let userLocation = [];

function Carrusel({location}, props) {
  const ref = useRef(null);
  userLocation = location;

  const goNext = () => {
    if (ref.current !== null && ref.current.swiper !== null) {
      ref.current.swiper.slideNext();
    }
  };

  const goPrev = () => {
    if (ref.current !== null && ref.current.swiper !== null) {
      ref.current.swiper.slidePrev();
    }
  };

  const params = {
    autoplay: {
      delay: 1000,
    },
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
    centeredSlides: true,
    pagination: false,
    speed: 1000,
    rebuildOnUpdate: true,
    
    navigation: {
      nextEl: '.swiper-btn-next',
      prevEl: '.swiper-btn-prev',
    },
    breakpoints: {
      350: {
        slidesPerView: 1.1,
        spaceBetween: 2,
      },
      375: {
        slidesPerView: 1.16,
        spaceBetween: 3,
      },
      415: {
        slidesPerView: 1.3,
        spaceBetween: 4,
      },
      475: {
        slidesPerView: 1.41,
        spaceBetween: 5,
      },
      515: {
        slidesPerView: 1.55,
        spaceBetween: 5,
      },

      575: {
        slidesPerView: 1.8,
        spaceBetween: 6,
      },

      615: {
        slidesPerView: 1.94,
        spaceBetween: 7,
      },

      675: {

        slidesPerView: 2.1,
        spaceBetween: 8,
      },

      745: {

        slidesPerView: 2.3,
        spaceBetween: 9,
      },

      800: {
        slidesPerView: 2.5,
        spaceBetween: 10,
      },

      900: {
        slidesPerView: 2.8,
        spaceBetween: 11,
      },

      1000: {
        slidesPerView: 3.1,
        spaceBetween: 20,
      },

      1100: {
        slidesPerView: 3.4,
        spaceBetween: 20,
      },

      1200: {
        slidesPerView: 3.6,
        spaceBetween: 24,
      },

      1300: {
        slidesPerView: 3.8,
        spaceBetween: 26,
      },

      1400: {
        slidesPerView: 4.5,
        spaceBetween: 15,
      },
      // 1600: {
      //   slidesPerView: 5,
      //   spaceBetween: 28,
      // },
    }
  }

  const goToItem = (idItem) => {
    //props.history.push("/item/"+ idItem)
    return  <Redirect  to="/item/"/>
  }

  let [lmiItems, saveItems] = useState([]);

  const fnGetNearmeItems = (
    userToken,
    accessKeyId,
    secretAccessKey,
    sessionToken
  ) => {
    try {
      console.log("user token: " + userToken);
      console.log("location: " + userLocation[0].toString());

      var apigClient = apigClientFactory.newClient({
        accessKey: accessKeyId,
        secretKey: secretAccessKey,
        sessionToken: sessionToken,
        region: "us-west-2",
      });

      var params = {};

      var body = {
        //This is where you define the body of the request
        //los angeles
        //  lat: (location.length > 0) ? location[0] : '29.0821369',
        //   lng: (location.length > 1) ? location[1] : '-111.0591982',
        lat: userLocation[0].toString(),
        lng: userLocation[1].toString(),
        //portland
        //  lat: "45.542627",
        //  lng: "-122.7947523",
        //Hermosillo
        // lat: "29.0821369",
        // lng: "-111.0591982",
        mileRadius: 80467.2,
        toc: userToken,
      };
      var additionalParams = {};

      apigClient
        .apiItemGetNearMeItemsPost(params, body, additionalParams)
        .then(function (result) {
          //debugger;
          //////debugger;
          //alert('anonimous result');
          console.log(JSON.parse(result.data));
          var object = JSON.parse(result.data);
          if (object.Error != "") {
          } else {
            if (object.Response[0] != null && object.Response[0] != undefined) {
              if (object.Response.length > 0) {
                //debugger;
                saveItems(object.Response);
              } else {
                alert("unable to get nearme items");
              }
            }
          }
        })
        .catch(function (error) {
          //debugger;
        });
    } catch (err) { }
  };

  const fnGetUserToken_Own_NoRegister = (
    sub,
    isFb,
    deviceId,
    isGuest,
    accessKeyId,
    secretAccessKey,
    sessionToken
  ) => {
    try {
      var apigClient = apigClientFactory.newClient({
        accessKey: accessKeyId,
        secretKey: secretAccessKey,
        sessionToken: sessionToken,
        region: "us-west-2",
      });

      var params = {};

      var body = {
        //This is where you define the body of the request
        cognitoID: "",
        isFb: 0,
        deviceId: "",
        isGuest: 1,
      };
      var additionalParams = {};

      apigClient
        .apiUserGetUserOauthTokenPost(params, body, additionalParams)
        .then(function (result) {
          console.log(JSON.parse(result.data));
          var object = JSON.parse(result.data);
          if (object.Error != "") {
          } else {
            if (object.Response[0] != null && object.Response[0] != undefined) {
              if (object.Response.length > 0) {
                const ownTok = object.Response[0].ownToke;

                if (ownTok.length > 0) {
                  fnGetNearmeItems(
                    ownTok,
                    accessKeyId,
                    secretAccessKey,
                    sessionToken
                  );
                } else {
                  alert("unable to get user token");
                }
              } else {
                alert("unable to get user token");
              }
            }
          }
        })
        .catch(function (error) {
          //debugger;
        });
    } catch (err) {
      //debugger;
    }
  };

  const fnContniueAnonimus = () => {
    try {
      AWS.config.region = "us-west-2";
      // Configure the credentials provider to use your identity pool
      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: "us-west-2:843a7f97-183a-45be-9f13-ec9b239f07a0",
      });
      // Make the call to obtain credentials
      AWS.config.credentials.get(function () {
        // Credentials will be available when this function is called.
        //alert('anonimus');
        var accessKeyId = AWS.config.credentials.accessKeyId;
        var secretAccessKey = AWS.config.credentials.secretAccessKey;
        var sessionToken = AWS.config.credentials.sessionToken;
        //AWS.config.credentials.refresh();
        if (
          AWS.config.credentials.accessKeyId == undefined ||
          AWS.config.credentials.accessKeyId == null
        ) {
          AWS.config.region = "us-west-2";
          // Configure the credentials provider to use your identity pool
          AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: "us-west-2:843a7f97-183a-45be-9f13-ec9b239f07a0",
          });
          AWS.config.credentials.get(function () {
            accessKeyId = AWS.config.credentials.accessKeyId;
            secretAccessKey = AWS.config.credentials.secretAccessKey;
            sessionToken = AWS.config.credentials.sessionToken;
            if (
              AWS.config.credentials.accessKeyId == undefined ||
              AWS.config.credentials.accessKeyId == null
            ) {
              AWS.config.credentials.get(function () {
                accessKeyId = AWS.config.credentials.accessKeyId;
                secretAccessKey = AWS.config.credentials.secretAccessKey;
                sessionToken = AWS.config.credentials.sessionToken;
                if (
                  AWS.config.credentials.accessKeyId != undefined ||
                  AWS.config.credentials.accessKeyId != null
                ) {
                  fnGetUserToken_Own_NoRegister(
                    "",
                    0,
                    "",
                    1,
                    accessKeyId,
                    secretAccessKey,
                    sessionToken
                  );
                }
              });
            } else {
              fnGetUserToken_Own_NoRegister(
                "",
                0,
                "",
                1,
                accessKeyId,
                secretAccessKey,
                sessionToken
              );
            }
          });
        } else {
          fnGetUserToken_Own_NoRegister(
            "",
            0,
            "",
            1,
            accessKeyId,
            secretAccessKey,
            sessionToken
          );
        }
      });
    } catch (err) { }
  };

  useEffect(() => {
    fnContniueAnonimus();
  }, [location]);


  return (
    <section id="lmi-carrusel" className="carrusel lmi-animation ">
      <div className="maxwidth">
        <div className="d-flex">
          <div className="subtitle">
            Items you can rent on <span>lend me it</span>
          </div>
          <div className="btn-carrusel ml-auto">
            <button type="button" className="btn swiper-btn-prev" onClick={goPrev}><i className="icon-left "></i></button>
            <button type="button" className="btn swiper-btn-next" onClick={goNext}><i className="icon-right"></i></button>
          </div>
        </div>
      </div>
      <div className="maxwidth">
        <Swiper {...params} ref={ref}>
          {lmiItems.map((item, index) => (
              <div className="card" key={index} onClick={() => goToItem(item.idCatLndItem)}>
                <div className="card-img">
                  <img alt={item.name} src={[item.itemPicture]} width="100%" />
                  {/* <div className="card-price">${item.dailyRentalPrice}</div> */}
                </div>
                <div className="card-name"> {item.name}</div>
                <div className="card-info"> 
                  <span>${item.dailyRentalPrice}</span>
                  <span> {item.categoryDesc}</span>
                </div>
                {/* <div className="card-locate"><img src={market} alt="Marker" /> {item.dailyRentalPrice}</div> */}
                <hr></hr>
                <div className="card-description">
                  {item.dscription}
                </div>
              </div>
          ))}
        </Swiper>
      </div>
    </section>
  );
};

export default Carrusel;