/* eslint-disable no-undef */
import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { withRouter } from 'react-router-dom';

import "./content-results.scss";

function ContentResults(props) {

  const goToItem = (idItem) => {
    props.history.push("/item/"+ idItem)
  }
  let [lmiItems, saveItems] = useState([]);
  let searchList = [];

  console.log(searchList.length);

  const fnGetSearchItems = (
    userToken,
    accessKeyId,
    secretAccessKey,
    sessionToken
  ) => {
    try {
      console.log("user token ");
      console.log(userToken);

      var apigClient = apigClientFactory.newClient({
        accessKey: accessKeyId,
        secretKey: secretAccessKey,
        sessionToken: sessionToken,
        region: "us-west-2",
      });

      var params = {};

      var body = {
        //This is where you define the body of the request
        lat: "29.0821369",
        lng: "-111.0591982",
        mileRadius: 80467.2,
        toc: userToken,
        generalSearch: props.location.pathname.slice(8, 500),
        page: 1,
        pageSize: 30
      };
      var additionalParams = {};

      apigClient
        .apiItemMainItemSearchNewFlowPost(params, body, additionalParams)
        .then(function (result) {
          //debugger;
          //////debugger;
          //alert('anonimous result');
          console.log(JSON.parse(result.data));
          var object = JSON.parse(result.data);
          if (object.Error != "") {
          } else {
            if (object.Response[0] != null && object.Response[0] != undefined) {
              if (object.Response.length > 0) {
                //debugger;
                saveItems(object.Response);
                localStorage.setItem('searchList', JSON.stringify(object.Response));
              } else {
                alert("unable to get nearme items");
              }
            }
          }
        })
        .catch(function (error) {
          //debugger;
        });
    } catch (err) { }
  };

  const fnGetUserToken_Own_NoRegister = (
    sub,
    isFb,
    deviceId,
    isGuest,
    accessKeyId,
    secretAccessKey,
    sessionToken
  ) => {
    try {
      var apigClient = apigClientFactory.newClient({
        accessKey: accessKeyId,
        secretKey: secretAccessKey,
        sessionToken: sessionToken,
        region: "us-west-2",
      });

      var params = {};

      var body = {
        //This is where you define the body of the request
        cognitoID: "",
        isFb: 0,
        deviceId: "",
        isGuest: 1,
      };
      var additionalParams = {};

      apigClient
        .apiUserGetUserOauthTokenPost(params, body, additionalParams)
        .then(function (result) {
          console.log(JSON.parse(result.data));
          var object = JSON.parse(result.data);
          if (object.Error != "") {
          } else {
            if (object.Response[0] != null && object.Response[0] != undefined) {
              if (object.Response.length > 0) {
                const ownTok = object.Response[0].ownToke;

                if (ownTok.length > 0) {
                  fnGetSearchItems(
                    ownTok,
                    accessKeyId,
                    secretAccessKey,
                    sessionToken
                  );
                } else {
                  alert("unable to get user token");
                }
              } else {
                alert("unable to get user token");
              }
            }
          }
        })
        .catch(function (error) {
          //debugger;
        });
    } catch (err) {
      //debugger;
    }
  };

  const fnContniueAnonimus = () => {
    try {
      AWS.config.region = "us-west-2";
      // Configure the credentials provider to use your identity pool
      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: "us-west-2:843a7f97-183a-45be-9f13-ec9b239f07a0",
      });
      // Make the call to obtain credentials
      AWS.config.credentials.get(function () {
        // Credentials will be available when this function is called.
        //alert('anonimus');
        var accessKeyId = AWS.config.credentials.accessKeyId;
        var secretAccessKey = AWS.config.credentials.secretAccessKey;
        var sessionToken = AWS.config.credentials.sessionToken;
        //AWS.config.credentials.refresh();
        if (
          AWS.config.credentials.accessKeyId == undefined ||
          AWS.config.credentials.accessKeyId == null
        ) {
          AWS.config.region = "us-west-2";
          // Configure the credentials provider to use your identity pool
          AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: "us-west-2:843a7f97-183a-45be-9f13-ec9b239f07a0",
          });
          AWS.config.credentials.get(function () {
            accessKeyId = AWS.config.credentials.accessKeyId;
            secretAccessKey = AWS.config.credentials.secretAccessKey;
            sessionToken = AWS.config.credentials.sessionToken;
            if (
              AWS.config.credentials.accessKeyId == undefined ||
              AWS.config.credentials.accessKeyId == null
            ) {
              AWS.config.credentials.get(function () {
                accessKeyId = AWS.config.credentials.accessKeyId;
                secretAccessKey = AWS.config.credentials.secretAccessKey;
                sessionToken = AWS.config.credentials.sessionToken;
                if (
                  AWS.config.credentials.accessKeyId != undefined ||
                  AWS.config.credentials.accessKeyId != null
                ) {
                  fnGetUserToken_Own_NoRegister(
                    "",
                    0,
                    "",
                    1,
                    accessKeyId,
                    secretAccessKey,
                    sessionToken
                  );
                }
              });
            } else {
              fnGetUserToken_Own_NoRegister(
                "",
                0,
                "",
                1,
                accessKeyId,
                secretAccessKey,
                sessionToken
              );
            }
          });
        } else {
          fnGetUserToken_Own_NoRegister(
            "",
            0,
            "",
            1,
            accessKeyId,
            secretAccessKey,
            sessionToken
          );
        }
      });
    } catch (err) { }
  };

 

  useEffect(() => {
    fnContniueAnonimus();
  }, [props.location]);

  searchList = JSON.parse(localStorage.getItem('searchList'));
  console.log(searchList);

  if(lmiItems.length > 0) {
    return (
      <Container className="results">
        <div className="item  mt-3">
  <b className="title">Searched for “{props.location.pathname.slice(8, 500)}” ({lmiItems.length})</b>
          {/* <p className="info">Four results for  </p> */}
        </div>
        <Row>
          {lmiItems.map((item, index) => (
            <Card key={index} onClick={() => goToItem(item.idCatLndItem)}>
              <div className="crop">
                <Card.Img src={[item.itemPicture]}/>
                <span className="price">${item.dailyRentalPrice}</span>
              </div>
              <div className="name">{item.name}</div>
            </Card>
          ))}
        </Row>
      </Container>
  );
  } else if(searchList != null && searchList.length > 0) {
    return (
      <Container className="results">
        <div className="item  mt-3">
  <b className="title">Searched for “{props.location.pathname.slice(8, 500)}” ({searchList.length})</b>
          {/* <p className="info">Four results for  </p> */}
        </div>
        <Row>
          {searchList.map((item, index) => (
            <Card key={index} onClick={() => goToItem(item.idCatLndItem)}>
              <div className="crop">
                <Card.Img src={[item.itemPicture]}/>
                <span className="price">${item.dailyRentalPrice}</span>
              </div>
              <div className="name">{item.name}</div>
            </Card>
          ))}
        </Row>
      </Container>
  );
  
  } else{
    return(
      <Container className="results">
        <div className="item  mt-3">
  <b className="title">Searched for “{props.location.pathname.slice(8, 500)}” (0)</b>
          {/* <p className="info">Four results for  </p> */}
        </div>
        <Row>
          <p>NO HAY ELEMENTOS</p>
        </Row>
      </Container>
    );
  }
  
}

export default withRouter(ContentResults);
