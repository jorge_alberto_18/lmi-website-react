import React, { Component } from "react";
import covid_character from '../../img/covid/covid_character.png';
import covid_icon from '../../img/covid/heart_cross.svg';

import {  Modal, ModalHeader, ModalBody } from 'reactstrap';


import "./covid19.scss";



class Covid19 extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.state = {
      showModal: false,
      smallButton: false,
      hideButton: false
    }
  }

  componentWillMount() {
    this.setState({ hideButton: true })
  }

  componentDidMount() {
    this.setState({ hideButton: false })
  }


  handleShow() {
    this.setState({ showModal: true, hideButton: true })
    setTimeout(() => {
      this.setState({ smallButton: true })
    }, 1000);
  }

  handleClose() {
    this.setState({ showModal: false, hideButton: false })
  }

  render() {

    const closeBtn = <button className="close" onClick={this.handleClose}>&times;</button>;
    const hideButton = (this.state.hideButton) ? " hideButton " : "";
    const smallButton = (this.state.smallButton) ? " smallButton " : "";

    const showCovid = this.props.showCovid;

    return (
      <section id="lmi-covid-19" className="covid19">
        {showCovid ? (
          <button onClick={this.handleShow} className={"btn-covid19 " + hideButton + smallButton} >
            <img src={covid_icon} alt="Icon COVID-19" width="30px" height="25px" />
            <span>Safety and Caution Regarding COVID-19</span>
          </button>
        ) : ""}
        <Modal isOpen={this.state.showModal} centered={true} className="modal-covid19">
          <ModalHeader close={closeBtn}>
            <img src={covid_icon} alt="Safety and Caution" height="45px" />
          Safety and Caution Regarding COVID-19
        </ModalHeader>
          <ModalBody>
            <div className="banner">
              <img src={covid_character} />
              <b>Safety and Caution Regarding COVID-19</b>
            </div>
            <div className="info">
              <p>The health and safety of our community is our priority and we ask that our users be mindful and follow the instructions from public health authorities regarding social distancing. </p>
              <p>Lend Me It is a new peer-to-peer app designed for lending and renting anything and everything. It gives users instant access to additional streams of income, as well as access to items that might be too large to store or too expensive to purchase. Lend Me It also addresses the issue of consumer waste. </p>
              <p>You can practice safety precautions during the rental process as you would when shopping in a store such as wearing masks, wiping down items with disinfectant before and after you use them, and maintaining a safe distance from others during transactions. Communicate with others regarding these safe practices easily via Lend Me It chat before or during the reservation pickup process.</p>


              <div className="btn-close-modal">
                <button onClick={this.handleClose} >
                  CLOSE
            </button>
              </div>
            </div>

          </ModalBody>
        </Modal>
      </section>
    );

  }


}




export default Covid19;