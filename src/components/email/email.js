import React, { Component } from "react";

import bg_email_md from '../../img/get-email/bg-email-md-min.jpg';
import bg_email_sm from '../../img/get-email/bg-email-sm.jpg';
import MailchimpSubscribe from 'react-mailchimp-subscribe'


import "./email.scss";



const CustomForm = ({ status, message, onValidated }) => {
  let email;
  const submit = () =>
    email &&
    email.value.indexOf("@") > -1 &&
    onValidated({
      EMAIL: email.value,
    });

  return (
    <div>
      <div className="d-flex lmi-animation">
        <input ref={node => (email = node)} type="email" placeholder="Your email" />
        <button onClick={submit}> Submit </button>
      </div>
      {status === "sending" && <div className="mce-responses">sending...</div>}
      {status === "error" && (<div className="mce-responses" dangerouslySetInnerHTML={{ __html: message }}/>)}
      {status === "success" && (<div className="mce-responses"  dangerouslySetInnerHTML={{ __html: message }}/>)}
    </div>

  );
};


class Email extends Component {

  render() {

    const url = "https://lendmeit.us19.list-manage.com/subscribe/post?u=4cda00e301f75163f28557cfa&amp;id=c0c53e528d"
    return (
      <section id="lmi-updates-email" className="updates-email lmi-animation">
        <div className="updates-email-img">
          <img src={bg_email_md} />
          <img src={bg_email_sm} />
        </div>
        <div className="overlay"></div>
        <div className="updates-email-form">
          <div className="content-updates-email">
            <p className="lmi-animation">Sign up for news, updates and promotions</p>
            <MailchimpSubscribe
              url={url}
              render={({ subscribe, status, message }) => (
                <CustomForm
                  status={status}
                  message={message}
                  onValidated={formData => subscribe(formData)}
                />
              )}
            />
          </div>
        </div>


      </section>
    );
  }
}


export default Email;