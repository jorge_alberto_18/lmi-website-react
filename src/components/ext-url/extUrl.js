import React, { Component } from "react";

import img_logo from '../../img/logo2.svg';
import getPosts from '../GetData'
import axios from 'axios'

import "./extUrl.scss";


class extUrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      posts: [],
    }
  }

  fetchFirst(url) {
    if (url) {
      fetch('https://s3-us-west-2.amazonaws.com/lendmt-docs/docs/' + url + '.html').then(function (response) {
        return response.text();
      }).then(function (result) {
        document.getElementById('htmlContainer').innerHTML = result;
      });
    }
  }
  componentWillMount() {
    if (this.props.path.pathname === "/faq") {
      this.fetchFirst("faqOneFive");
    }
    if (this.props.path.pathname === "/terms") {
      this.fetchFirst("termsAndConditionsOneFive");
    }
    if (this.props.path.pathname === "/privacy-policy") {
      this.fetchFirst("privacyPolicy");
    }
    if (this.props.path.pathname === "/eula") {
      this.fetchFirst("eulamOneFive");
    }
    if (this.props.path.pathname === "/about") {
      this.fetchFirst("aboutOneFive");
    }
    if (this.props.path.pathname === "/guarantee") {
      this.fetchFirst("guaranteeOneFive");
    }
  }


  render() { 
    return (
      <section>
        <div className="logo">
                <img src={img_logo} height="62" alt="" />
            </div>
        <div id="htmlContainer"></div>
      </section>
    );
  }
}

export default extUrl;