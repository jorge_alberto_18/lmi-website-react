import React, { Component } from "react";
import { Link } from 'react-router-dom';

import "./faq.scss";


class extUrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      categories: [],
      activeLinkCat: -1,
      idFaqs: 5
    }
    this.posts();
  }


  componentWillReceiveProps(props) {
    const { search } = props;
    var replaced = search.split(' ').join('%20');
    this.posts(replaced)
  }

  posts(query) {
    var search = ''
    if (query) { search = "&search=" + query }
    // let postUrl = getPosts+"posts?categories=2&orderby=id&order=asc&per_page=100"+search;
    fetch("https://lendmeit.com/support/lmisuppla/wp-json/wp/v2/posts?categories=2&orderby=id&order=asc&per_page=100" + search)
      .then(data => data.json())
      .then(data => {
        this.setState({
          posts: data
        })
      }).catch(data => console.log(data))

    fetch('https://lendmeit.com/support/lmisuppla/wp-json/wp/v2/categories?hide_empty=true&per_page=20')
      .then(data => data.json())
      .then(result => {
        this.setState({
          categories: result
        })
      }).catch(data => console.log(data))
  }

  goToCat(slug, index) {
    window.scrollTo({
      top: document.getElementById(slug).offsetTop - 55,
      behavior: "smooth"
    });

    this.setState({ activeLinkCat: index });
  }

  render() {
    let letActive = this.state.activeLinkCat

    if (this.state.posts.length && this.state.categories.length) {
      return (
        <div className="content-support">
          <section className="support">
            <div className="wp-categories">
              {this.state.categories.map((sub, index) => {
                if (sub.parent == 2) {
                  return (
                    <div className="cLink" key={index}>
                    <Link to="/faq" onClick={(e) => this.goToCat(sub.slug, index)}
                      {...letActive == -1 ? letActive = index : ''}
                      className={letActive === index ? 'link-cat active' : 'link-cat'}> {sub.name} </Link>
                      </div>
                  )
                }
              })}

            </div>
            <div className="wp-posts">
              {this.state.categories.map((sub, index) => {
                if (sub.parent == 2) {
                  return (
                    <div className="box" key={index}>
                      <h4 id={sub.slug}>{sub.name}</h4>
                      {this.state.posts.map((post, key) => {
                        return post.categories.map(cat => {
                          if (cat == sub.id) {
                            return (
                              <div id={post.slug} key={key}>
                                <b>{post.title.rendered}</b>
                                {<p dangerouslySetInnerHTML={{ __html: post.content.rendered }} />}
                              </div>
                            )
                          }
                        });
                      })
                      }
                    </div>
                  )
                }
              })}

            </div>
          </section>
        </div>
      );
    } else {
      return (
        <div className="content-support">
        <section className="support">
          <div className="loader"></div>
          </section>
        </div>
      )
    }



  }
}


export default extUrl;