import React from "react";

import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css'

import img_header_md1 from '../../img/header/bg-header-md-min.jpg';
import img_header_sm1 from '../../img/header/header-sm-min.jpg';
import img_header_md2 from '../../img/header/bg-header-md-2-min.jpg';
import img_header_sm2 from '../../img/header/header-sm-2-min.jpg';
import img_header_md3 from '../../img/header/bg-header-md-3-min.jpg';
import img_header_sm3 from '../../img/header/header-sm-3-min.jpg';


import appStore from '../../img/header/app-store-badge.svg';
import googlePlay from '../../img/header/google-play-badge.svg';

import "./header.scss";



const header = (props) =>  {

  const params = {
    loop: true,
    pagination: false,
    speed: 1000,
    autoHeight: true,   
    centeredSlides: true,
    // lazy: true,
    autoplay: {
        delay: 12000,
        disableOnInteraction: true,
    },
    fadeEffect: {
        crossFade: true
    },
    effect: "fade"
  }

  let secondImage
    if(props.start){
      if(props.isMobile){
        secondImage = <img src={img_header_md1 } className={"img-md" } alt="Md" />
      }else{
        secondImage = <img src={img_header_sm1 } className={"img-sm" } alt="Sm" />
      }
    }
    return (
      <header id="lmi-header" className="lmi-animation maxwidth pt-3">
        <div className="lmi-slider">
          {/* <Swiper {...params}>
            <div>
              <img src={img_header_md1} alt="Banner1" onLoad={() => props.onLoadBanner(1)} className="img-md" />
              <img src={img_header_sm1} alt="Banner1" onLoad={() => props.onLoadBanner(2)} className="img-sm" />
            </div>
            <div>
              <img src={img_header_md2} alt="Banner2" className="img-md right" />
              <img src={img_header_sm2} alt="Banner2" className="img-sm right" />
            </div>
            <div>
              <img src={img_header_md3} alt="Banner3" className="img-md" />
              <img src={img_header_sm3} alt="Banner3" className="img-sm" />
            </div>
          </Swiper> */}
          <div>
              <img src={!props.isMobile ? img_header_md1 : img_header_sm1 } 
              className={!props.isMobile ? "img-md" : "img-sm" }
              alt="Banner1" onLoad={() => props.onLoadBanner()} 
               />
               {secondImage}
               {/* <img src={!props.start ? img_header_md1 : img_header_sm1 } 
              className={!props.start ? "img-md" : "img-sm" }
              alt="Banner1" /> */}
              {/* <img src={img_header_sm1} alt="Banner1" onLoad={() => props.onLoadBanner(2)} className="img-sm" /> */}
            </div>
        </div>
        {/* <div className="overlay"></div> */}
        {/* <div className="gradient"></div> */}
        <div className="caption">
          <div className="content-caption">
            <h1>Why buy when you can rent?</h1>
            <div className="download-app">
              <a href="https://play.google.com/store/apps/details?id=com.lendmeit.app" target="_blank">
                <img src={googlePlay} alt="googlePlay" />
              </a>
              <a href="https://itunes.apple.com/us/app/lend-me-it-lend-rent-easily/id1444352676" target="_blank">
                <img src={appStore} alt="appStore"/>
              </a>
            </div>
          </div>
        </div>
      </header>
    );
  }


//   const Slider = (props) => {

//     const params = {
//       loop: true,
//       pagination: false,
//       speed: 1000,
//       autoHeight: true,   
//       centeredSlides: true,
//       // lazy: true,
//       autoplay: {
//           delay: 12000,
//           disableOnInteraction: true,
//       },
//       fadeEffect: {
//           crossFade: true
//       },
//       effect: "fade"
//     }
    
//     return(      
//       <Swiper {...params}>
//         <div>
//           <img src={img_header_md1} onLoad={() => props.onLoadBanner()} className="img-md" />
//           <img src={img_header_sm1} onLoad={() => props.onLoadBanner()} className="img-sm" />
//         </div>
//         <div>
//           <img src={img_header_md2} className="img-md right" />
//           <img src={img_header_sm2} className="img-sm right" />
//         </div>
//         <div>
//           <img src={img_header_md3} className="img-md" />
//           <img src={img_header_sm3} className="img-sm" />
//         </div>
//       </Swiper>
//     )
// }

export default header;