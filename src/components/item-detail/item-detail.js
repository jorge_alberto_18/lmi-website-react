/* eslint-disable no-undef */
import React, { Component, useEffect, useState } from "react";
import { withRouter } from 'react-router-dom';
import bike from '../../img/slider-items/news/bike-min.jpg';
import canon from '../../img/slider-items/news/canon-min.jpg';
import classicalGuitar from '../../img/slider-items/news/classical-guitar-min.jpg';

import googlePlay from '../../img/header/google-play-badge.svg';
import appStore from '../../img/header/app-store-badge.svg';
import Carrusel from "../carrusel/carrusel";


import "./item-detail.scss";
import { Container, Col, Row } from "reactstrap";
import {  Route, Switch, Link,  } from 'react-router-dom';



class  ItemDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      img_select: '',
      active: ''
    };
  }
//   toggle(e){
//     this.setState({active: !this.state.active});
//  }

  changeImage (imgClick) {
    this.setState({ img_select: imgClick });
  }
  componentDidMount() {
    this.setState({ img_select: classicalGuitar });
  }

  render() {
  return (
    <Container>
      <Link to={`/search/`} className="back-results"><i className="fas fa-caret-left mr-3"></i> Back to search results</Link>
      <div className="item-detail">
        <div className="item-image">
          <div className="image-sm">
            <img id="1" src={classicalGuitar} onClick={() => this.changeImage(classicalGuitar)} width="100%" alt="" />
            <img id="2" src={bike} onClick={() => this.changeImage(bike)} width="100%" alt="" />
            <img id="3" src={canon} onClick={() => this.changeImage(canon)} width="100%" alt="" />
          </div>
          <div className="image-bg">
            <img src={this.state.img_select} width="100%" alt="" />
          </div>
          
        </div>
        <div className="item-info">
          <div className="item-name">Yamaha F310, 6-Strings Acoustic Guitar, Natural</div>
          <div className="item-description">
            Back/Side/Neck Material:Locally Sourced Tonewood (To minimize waste and 
            support sustainable procurement, various species of tonewoods are used 
            based on availability).
          </div>
          <hr/>
          <div className="item-content-col">
            <div className="item-col">
              <span>Rental price per day</span>
              <b>$25.00</b>
            </div>
            <div className="item-col">
              <span>Replacement Cost</span>
              <b>$105.00</b>
            </div>
          </div>

          <hr/>
          <div className="item-content-col">
            <div className="item-col">
              <span>Category</span>
              <b>Instruments</b>
            </div>
            <div className="item-col">
              <span>Condition</span>
              <b>Great</b>
            </div>
            <div className="item-col sm-hide">
              <span>Delivery Method</span>
              <b>Pickup only</b>
            </div>
          </div>
          <hr className="bg-hide"/>
          <div className="item-row bg-hide">
            <span>Delivery Method</span>
            <b>Pickup only</b>
          </div>
          <hr/>
          <div className="item-row">
            <span>Location</span>
            <b>Boston logan international airport</b>
          </div>
          <div className="download-app">
            <a href="https://play.google.com/store/apps/details?id=com.lendmeit.app" target="_blank">
              <img src={googlePlay} alt="Google Play" />
            </a>
            <a href="https://itunes.apple.com/us/app/lend-me-it-lend-rent-easily/id1444352676" target="_blank">
              <img src={appStore} alt="AppStore" />
            </a>
          </div>
        </div> 
      </div>
      <hr/>
      <Carrusel />
    </Container>
      
  );
            }
}

export default withRouter(ItemDetails);
