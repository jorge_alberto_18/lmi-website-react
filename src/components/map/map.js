import React, { Component } from "react";

import img_exploreApp from '../../img/explore_app-min.png';
import img_map from '../../img/map-min.png';
import pin1 from '../../img/pin-map/pin1.jpg';
import pin2 from '../../img/pin-map/pin2.jpg';
import pin3 from '../../img/pin-map/pin3.jpg';
import pin4 from '../../img/pin-map/pin4.jpg';
import pin5 from '../../img/pin-map/pin5.jpg';
import googlePlay from '../../img/header/google-play-badge.svg';
import appStore from '../../img/header/app-store-badge.svg';

import "./map.scss";

class Why extends Component {
  render() {
    return (
      <section id="lmi-map" className="map lmi-animation">
        <div className="parallax">
          <img src={img_map} alt="Map" className="parrallaxTop" />
          <div className="marker parrallaxTop">
            <img src={pin1} alt="Pin" />
            <img src={pin2} alt="Pin" />
            <img src={pin3} alt="Pin" />
            <img src={pin4} alt="Pin" />
            <img src={pin5} alt="Pin" />
          </div>
        </div>
        <div className="map-app-download lmi-animation">
          <div className="text"> Get it in the <br /> iOS App Store <span className="br-text"> or on Google Play today! </span></div>
          <div className="download-app">
            <a href="https://play.google.com/store/apps/details?id=com.lendmeit.app" target="_blank">
              <img src={googlePlay} alt="Google Play" />
            </a>
            <a href="https://itunes.apple.com/us/app/lend-me-it-lend-rent-easily/id1444352676" target="_blank">
              <img src={appStore} alt="AppStore" />
            </a>
          </div>
        </div>
        <div className="map-app lmi-animation"><img src={img_exploreApp} alt="Explore App" />
        </div>
      </section>
    );
  }
}


export default Why;