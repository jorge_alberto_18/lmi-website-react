import React, { Component } from "react";

import img_logo from '../../img/logo2.svg';
import appStore from '../../img/header/app-store-badge.svg';
import googlePlay from '../../img/header/google-play-badge.svg';
import { Link } from 'react-router-dom';
import Covid19 from '../covid19/covid19'


import "./menuLinks.scss";

class menuLinks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Company: false,
      Support: false,
      Other: false,
      Show: false,
    };
    this.toggleCompany = this.toggleCompany.bind(this);
    this.toggleSupport = this.toggleSupport.bind(this);
    this.toggleOther = this.toggleOther.bind(this);
    this.goToVideo = this.goToVideo.bind(this);
    this.toggleChat = this.toggleChat.bind(this);
  }

  toggleCompany() {
    this.setState({ Company: !this.state.Company });
    this.setState({ Show: true });
  };
  toggleSupport() {
    this.setState({ Support: !this.state.Support });
    this.setState({ Show: true });
  };
  toggleOther() {
    this.setState({ Other: !this.state.Other });
    this.setState({ Show: true });
  };
  toggleChat() { 
    window.zE('webWidget', 'toggle');
  }

  goToVideo() {  
    var topID = (document.getElementById('lmi-how-it-works').offsetTop)
    window.scrollTo({
      top: topID,
      behavior: "smooth"
    });
  }

  goToTop() {  
    window.scrollTo({
      top: 0,
    });
  } 

  ModalRef = ({handleShow}) => {
    this.showModal = handleShow;
  }
 
  openModal = () => {
   this.showModal();
  }

  render() {
    const Show = (this.state.Show) ? " show " : "";
    const Company = (this.state.Company) ? "open" : "";
    const Support = (this.state.Support) ? "open" : "";
    const Other = (this.state.Other) ? "open" : "";

    // const covidShowPath = (this.props.path === "/") ? true : false;
    return (
      <div>
          <section id="lmi-before-footer"  className="menuLinks lmi-animation">
        <div className="logo lmi-animation">
          <img src={img_logo} height="62" alt="" />
        </div>
        <div className="links lmi-animation">
          <ul className="menu">
            <li className="title-menu"> Company</li>
            <li><Link to={'/about'}>About</Link> </li>
            <li><a href="mailto:jobs@lendmeit.com">Jobs</a></li>
            <li><a href="mailto:contact@lendmeit.com">Contact</a></li>
          </ul>
          <ul className="menu">
            <li className="title-menu">Support</li>
            <li><Link to={'/faq'} onClick={this.goToTop}>FAQ</Link> </li>
            <li><Link to={'/how-it-works'} onClick={this.goToTop}>How it works</Link> </li>
            <li><a href="https://www.youtube.com/playlist?list=PLAprfj1Zv-1tOcecNKZh9LqC175p-09K8" target="_blank">Tutorials</a></li>
            <li><a onClick={this.toggleChat} > Customer Support</a ></li>
          </ul>
          <ul className="menu">
            <li className="title-menu">Other</li>
            <li><Link to={'/guarantee'}>Our Guarantee</Link> </li>
            <li><Link to={'/terms'}>Terms and Conditions</Link> </li>
            <li><Link to={'/privacy-policy'}>Privacy Policy</Link> </li>
            <li><Link to={'/eula'}>EULA</Link> </li>
          </ul>
        </div>
        <div className="accordion" >
          <div className={'card  ' + Company + Show}>
            <div className="title-menu" onClick={this.toggleCompany} >
              Company <i className="fas fa-chevron-up"></i>
            </div>
            <div className="content-collapse">
              <ul className="menu">
                <li><Link to={'/about'}>About</Link> </li>
                <li><a href="mailto:jobs@lendmeit.com">Jobs</a></li>
                <li><a href="mailto:contact@lendmeit.com">Contact</a></li>
              </ul>
            </div>
          </div>
          <div className={'card ' + Support + Show}>
            <div onClick={this.toggleSupport} className='title-menu'>
              Support <i className="fas fa-chevron-up"></i>
            </div>
            <div className="content-collapse">
              <ul className="menu">
                <li><Link to={'/faq'} onClick={this.goToTop}>FAQ</Link> </li>
                <li><Link to={'/how-it-works'} onClick={this.goToTop}>How it works</Link> </li>
                <li><a href="https://www.youtube.com/playlist?list=PLAprfj1Zv-1tOcecNKZh9LqC175p-09K8" target="_blank">Tutorials</a></li>
                <li><a onClick={this.toggleChat} > Customer Support</a></li>
              </ul>
            </div>
          </div>
          <div className={'card ' + Other + Show}>
            <div className="title-menu" onClick={this.toggleOther}>
              Other <i className="fas fa-chevron-up"></i>
            </div>
            <div className="content-collapse">
              <ul className="menu">
              <li><Link to={'/guarantee'}>Our Guarantee</Link> </li>
              <li><Link to={'/terms'}>Terms and Conditions</Link> </li>
              <li><Link to={'/privacy-policy'}>Privacy Policy</Link> </li>
              <li><Link to={'/eula'}>EULA</Link> </li>
              </ul>
            </div>
          </div>
        </div>
       
        {/* <div className="covid-banner" onClick={this.openModal}>
          COVID-19 Updates 
        </div> */}
        <div className="external-link lmi-animation">
          <div className="icons">
            <div className="circle">
              <a target="_blank" href="https://www.facebook.com/lendmeitapp/">
                <i className="fab fa-facebook-f"></i>
              </a>
            </div>
            <div className="circle">
              <a target="_blank" href="https://www.instagram.com/lendmeit/">
                <i className="fab fa-instagram"></i>
              </a>
            </div>
            <div className="circle">
              <a target="_blank" href="https://twitter.com/lendmeitapp/">
                <i className="fab fa-twitter"></i>
              </a>
            </div>
            <div className="circle">
              <a target="_blank" href="https://www.youtube.com/channel/UCsWJj7fzvJM786uTAstKTnQ/">
                <i className="fab fa-youtube"></i>
              </a>
            </div>
          </div>
          <div className="download-app">
            <a href="https://play.google.com/store/apps/details?id=com.lendmeit.app" target="_blank">
              <img src={googlePlay} />
            </a>
            <a href="https://itunes.apple.com/us/app/lend-me-it-lend-rent-easily/id1444352676" target="_blank">
              <img src={appStore} />
            </a>
          </div>
        </div>
      </section>
        {/* <Covid19 ref={this.ModalRef.bind(this)} showCovid={covidShowPath}></Covid19> */}
      </div>
    
      
    );
  }
}


export default menuLinks;