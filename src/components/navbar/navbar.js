import React, { Component } from "react";
import img_logo from '../../img/logo.svg';
import { Link } from 'react-router-dom';
import "./navbar.scss"; 
import SearchLocation from "../search-location/search-location";


class navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: false
    };
    this.toggleMenu = this.toggleMenu.bind(this);
    this.goToVideo = this.goToVideo.bind(this);
    this.toggleChat = this.toggleChat.bind(this);
  }
  toggleMenu() {
    this.setState({ menu: !this.state.menu })
  } 

  goToVideo() {  
    window.scrollTo({
      top: document.getElementById('lmi-how-it-works').offsetTop,
      behavior: "smooth"
    });
  } 
  

  toggleChat() { 
    window.zE('webWidget', 'toggle');
}

  render() {
    const show = (this.state.menu) ? "show" : "";
    return (
      <nav id="lmi-navbar" className="lmi-animation navbar navbar-expand-md navbar-light" >
        <div className="navbar-container">
          <a className="navbar-brand" href="/">
            <img src={img_logo} alt="Logo LMI" height="50.5" />
          </a>
          <SearchLocation onChange={() => console.log('SearchLocationChanged')}></SearchLocation>
          <div className={"navbar-collapse justify-content-end " + show} >
            <div className="navbar-nav ">
              <Link className="nav-item nav-link" to={'/faq'}>FAQ</Link>
              <Link className="nav-item nav-link" to={'/how-it-works'}>How it works</Link>
              <Link className="nav-item nav-link" to={'/guarantee'}>Our Guarantee</Link>
              <a className="nav-item nav-link" onClick={this.toggleChat}>Support</a>
            </div>
          </div>
          <button className="navbar-toggler ml-auto" type="button" onClick={this.toggleMenu}>
            <span className="line-menu"></span>
            <span className="line-menu"></span>
          </button>
        </div>
      </nav>
    );
  }
}



export default navbar;