import React, { Component } from "react";
import { Link } from 'react-router-dom';
import "./navbar2.scss";

class navbar2 extends Component {
  
  render() {
    
    return (
      <nav className="navbar-terms">
      <div className="content-terms">
          {/* <a href="/"><i className="fas fa-caret-left"></i> Back to homepage</a> */}
          <Link to={`/`}><i className="fas fa-caret-left"></i> Back to homepage</Link>

      </div>
  </nav>
    );
  }
}

export default navbar2;