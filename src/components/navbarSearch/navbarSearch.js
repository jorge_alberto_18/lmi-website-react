import React, { Component, useState, useEffect, useRef } from "react";
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import * as Icon from 'react-feather';
import img_logo from '../../img/logo2.svg';
import { Link } from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown'
import FormControl from 'react-bootstrap/FormControl'
import "./navbarSearch.scss";
import SearchLocation from "../search-location/search-location";
import { withRouter } from 'react-router-dom';
import { nouns } from '../../assets/nouns';


let autoComplete;

const loadScript = (url, callback) => {
  let script = document.createElement("script");
  script.type = "text/javascript";

  if (script.readyState) {
    script.onreadystatechange = function() {
      if (script.readyState === "loaded" || script.readyState === "complete") {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    script.onload = () => callback();
  }

  script.src = url;
  document.getElementsByTagName("head")[0].appendChild(script);
};


// const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
//   <a href="" ref={ref} onClick={(e) => {
//       e.preventDefault();
//       onClick(e);
//     }}
//   >
//     {children}
//   </a>
// ));

// const CustomMenu = React.forwardRef(
//   ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
//     const [query, setQuery] = useState("");
//     const autoCompleteRef = useRef(null);

//     useEffect(() => {
//       loadScript(
//         `https://maps.googleapis.com/maps/api/js?key=AIzaSyBTGEl38pnb8JAaPpTJaFGngss8P7MUW4c&libraries=places`,
//         () => handleScriptLoad(setQuery, autoCompleteRef)
//       );
//     }, []);

//     return (
//       <div
//         ref={ref}
//         style={style}
//         className={className}
//         aria-labelledby={labeledBy}
//       >
//         <FormControl
//           autoFocus
//           className="mx-3 my-2 w-auto"
//           placeholder="Type your location..."
//           onChange={(e) => setQuery(e.target.value)}
//           value={query}
//         />
//         <ul className="list-unstyled">
//           {React.Children.toArray(children).filter(
//             (child) =>
//               !query || child.props.children.toLowerCase().startsWith(query),
//           )}
//         </ul>
//       </div>
//     );
//   },
// );

class navbarSearch extends Component {
  constructor(props) {
    super(props);
    const { fnGetLocation } = this.props;
    this.state = {
      show: false,
      saveLocation: fnGetLocation,
      query: React.createRef(),
      suggestionsActive: false,
      overlayClick: false,
      suggestions: [],
      history: []
    }
  }

  onClickInput() {
    this.setState({ suggestionsActive: true });
    this.setState({ overlayClick: true });
  }

  onClickOverlay() {
    this.setState({ suggestionsActive: false });
    this.setState({ overlayClick: false });
  }
  
  showLocationModal = () => this.setState({show: true});
  hideLocationModal = () => this.setState({show: false});

  goToResults(search) {
    this.props.history.push("/search/" + search.split(' ').join('-'))
  }

  handleInputSearchChange = () => {
    this.setState({
      query: this.search.value
    }, () => {
      if (this.state.query && this.state.query.length > 1) {
        // if (this.state.query.length % 2 === 0) {
          this.getSuggestions();
        // }
      }
    });
  }

  getSuggestions = () => {
    let resultList = []; 
    nouns.filter(noun => noun.startsWith(this.state.query)).map((data, key) => {
      resultList.push(data);
    });
    
    this.setState({suggestions: resultList.slice(0, 5)});
  }

  render() {

    const handleKeyDown = (event) => {
      if (event.key === 'Enter') {
        this.goToResults(this.search.value);
      }
    }
    
    let showSuggestions
    if (this.state.suggestionsActive && this.state.suggestions.length) { 
    showSuggestions = <div className="suggestions" onClick={() => this.onClickOverlay()}>
        <div className="title">SEARCH SUGGESTIONS</div>
        {this.state.suggestions.map((res, key) => {
          return (
            <div className="item" key={key} onClick={() => this.goToResults(res)}>
              <Icon.Search size={18} />
              <div className="suggestionsName">{res}</div>
            </div>
          );
        })}
      </div>
    }
    
    let overlay
    if (this.state.overlayClick) {
      overlay =
        <div onClick={() => this.onClickOverlay()} className="overlaySuggestions "></div>
    }

    // $('.modal-searchLocation .pac-item').onClick(function() {
    //   $('.modal-searchLocation ').modal('hide');
    // });

    return (
      <div className="navbarSearch">
        <div className="navbar-top">
        <a className="mr-5" href="/">
            <img src={img_logo} alt="Logo LMI" height="48" />
          </a>
          <a href="#" className='btn-location ml-auto' onClick={this.showLocationModal}>
            <Icon.MapPin size={14} /> 
            <small className="ml-1 mr-3"> {this.props.currentLocation}</small>
            <Icon.ChevronDown size={14} />
          </a>
        </div>
        <nav className="navbarSearchContent">
          <a className="mr-5" href="/">
            <img src={img_logo} alt="Logo LMI" height="48" />
          </a>
          <div className="content-search">
            <div className="icon-input" >
              <input autoComplete="on" type="text" 
              placeholder="What are you looking to rent?" 
              onClick={() => this.onClickInput()}
              // onKeyDown={handleKeyDown} 
              ref={input => this.search = input}
              onChange={this.handleInputSearchChange}
              />
              <Icon.Search size={18} />
            </div>
            {showSuggestions}
            {overlay}
          </div>
            <a href="#" className='btn-location ml-5' onClick={this.showLocationModal}>
              <Icon.MapPin size={14} /> 
              <small className="ml-1 mr-3"> {this.props.currentLocation}</small>
              <Icon.ChevronDown size={14} />
            </a>
            <Modal isOpen={this.state.show} toggle={this.hideLocationModal} centered={true} className="modal-searchLocation" >
              <ModalHeader>
                Search for new location
              </ModalHeader>
              <ModalBody>
                <SearchLocation saveLocation={this.state.saveLocation}/>
              </ModalBody>
              <ModalFooter  className="btn-close-modal">
              <button onClick={this.hideLocationModal}>
                      CLOSE
              </button>
              </ModalFooter>
            </Modal>
        </nav>
      </div>
    );
  }
}


export default withRouter(navbarSearch);