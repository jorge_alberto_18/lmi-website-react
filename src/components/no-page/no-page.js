import React, { Component } from "react"; 
import "./no-page.scss"

class noPage extends Component {
  render() {
    return (
      <div className="no-page"> 
          <h2>:(</h2>
          <h1>404</h1>
          <h5>OOPS, The page you are looking for can´t be found!</h5>
      </div>
    );
  }
}
export default noPage;