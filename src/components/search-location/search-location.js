import React, { useState, useEffect, useRef } from "react";
import "./search-location.scss";

import * as Icon from 'react-feather';

let autoComplete;

const loadScript = (url, callback) => {
  let script = document.createElement("script");
  script.type = "text/javascript";

  if (script.readyState) {
    script.onreadystatechange = function() {
      if (script.readyState === "loaded" || script.readyState === "complete") {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    script.onload = () => callback();
  }

  script.src = url;
  document.getElementsByTagName("head")[0].appendChild(script);
};

function handleScriptLoad(updateQuery, autoCompleteRef, saveLocation) {
  autoComplete = new window.google.maps.places.Autocomplete(
    autoCompleteRef.current,
    { 
      types: ["(cities)"], 
      // componentRestrictions: { country: "us" } 
    }
  );
  autoComplete.setFields(["address_components", "formatted_address", "geometry"]);
  autoComplete.addListener("place_changed", () =>
    handlePlaceSelect(updateQuery, saveLocation)
  );
}

async function handlePlaceSelect(updateQuery, saveLocation) {
  const addressObject = autoComplete.getPlace();
  const query = addressObject.formatted_address;
  updateQuery(query);

  saveLocation([
    addressObject.geometry.location.lat().toString(), 
    addressObject.geometry.location.lng().toString(), 
    query
  ]);
}

const  SearchLocation = ({saveLocation}) => {
  const [query, setQuery] = useState("");

  const autoCompleteRef = useRef(null);
  
 

  useEffect(() => {
    loadScript(
      `https://maps.googleapis.com/maps/api/js?key=AIzaSyBTGEl38pnb8JAaPpTJaFGngss8P7MUW4c&libraries=places`,
      () => handleScriptLoad(setQuery, autoCompleteRef, saveLocation)
    );
  }, []);

  return (
<div className="search-location">
    <div className="content-search-location">
      <div className="icon-input" >
        <input
            ref={autoCompleteRef}
            onChange={event => setQuery(event.target.value)}
            placeholder="Search Location"
            value={query}
        />
        <Icon.Search size={18} />
      </div>
    </div>
</div>
  );
}

export default SearchLocation;