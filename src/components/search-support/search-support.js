import React from "react";

import img_header_md1 from '../../img/header/bg-search.jpg';
import img_header_sm1 from '../../img/header/bg-search-sm.jpg';

// import axios from 'axios'
// import API_URL from '../GetData'
import "./search-support.scss";
import * as Icon from 'react-feather';


// class searchSupport extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       query: '',
//       results: []
//     }

//   }

//   getInfo = () => {
//     axios.get(`${API_URL}?search="${this.state.query}"`)
//       .then(({ data }) => {
//         this.setState({
//           results: data
//         })
//       })
//   }
//   handleInputChange = () => {
//     var replaced = this.search.value.split(' ').join('%20');
//     this.setState({
//       query: replaced
//     }, () => {
//       if (this.state.query && this.state.query.length > 1) {
//         if (this.state.query.length % 2 === 0) {
//           this.getInfo()
//         }
//       }
//     })
//   }

//   textSearch = () => {
//     this.results = this.search.value;
//     console.log("here: " +  this.results)
//   }

//   render() {

//     return (
//       <section id="lmi-search-support" className="search-support">
//         <div className="bg-search-support">
//           <img src={img_header_md1} />
//           <img src={img_header_sm1} />
//         </div>
//         <div className="overlay"></div>
//         <div className="info-search-support">
//           <div className="content-search-support">
//             <h1 className="text-center">Problems?</h1>
//             <p className="text-center">We are here to help you</p>
//             <div className="d-flex lmi-animation">
//                <input type="text" ref={input => this.search = input} onChange={this.textSearch} />
//               <button type="submit" className="btn btn-outline-light">Search</button>
//             </div>
//           </div>
//         </div>
//       </section>
//     );
//   }
// };


const boxSearch = (props) => {
var inputRef = React.createRef()
//var query = ''
// var textSearch = () => {
//   query = inputRef.value;
// }

  return (
    <section id="lmi-search-support" className="search-support">
      <div className="bg-search-support">
        <img src={img_header_md1} />
        <img src={img_header_sm1} />
      </div>
      <div className="overlay"></div>
      <div className="info-search-support">
        <div className="content-search-support">
          <h1 className="text-center">How it works</h1>
          <p className="text-center">How we can help you?</p>
            <div className="icon-input">
              <input type="text" placeholder ="Type your question" ref={input => inputRef = input} onChange={()=> props.handleClick(inputRef.value)} />
              <Icon.Search  size={18} />
            </div>
            {/* <button type="submit" className="btn btn-outline-light" onClick={()=> props.handleClick(inputRef.value)}>Search</button> */}
          </div>
      </div>
    </section>
  );
}

export default boxSearch;