import React, { Component } from "react";

import img_header_md1 from '../../img/header/bg-search.jpg';
import img_header_sm1 from '../../img/header/bg-search-sm.jpg';

import "./search.scss";
import * as Icon from 'react-feather';
import { withRouter } from 'react-router-dom';

class searchSupport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: React.createRef(),
      suggestionsActive: false,
      overlayClick: false,
      textTittle: '',
      results: []

    }

    // this.handleClick = this.handleClick.bind(this)
    this.goToPost = this.goToPost.bind(this)
    this.textSearch = this.textSearch.bind(this)

    const { path } = this.props.match;
    if (path === '/faq') {
      this.state.textTittle = 'FAQ'
    }
    if (path === '/how-it-works') {
      this.state.textTittle = "How it works"
    }
  }


  textSearch() {
    var replaced = this.state.query.value.split(' ').join('%20');

    fetch('https://lendmeit.com/support/lmisuppla/wp-json/wp/v2/posts?orderby=id&order=asc&per_page=5&categories=' + this.props.idCatParent + '&search=' + replaced)
      .then(data => data.json())
      .then(result => {
        this.setState({
          results: result
        })
      }).catch(data => console.log(data))
    this.setState({ suggestionsActive: true });
  }


  goToPost(slug) {
    if(this.props.path === '/faq'){
      window.scrollTo({
        top: document.getElementById(slug).offsetTop,
        behavior: "smooth"
      });
    }
    if (this.props.path === '/how-it-works') {
      this.props.history.push("/how-it-works/post/" + slug)
    }
  }

  goToResults(search) {
    this.props.history.push("/how-it-works/search/" + search.split(' ').join('-'))
  }

  onClickInput() {
    this.setState({ suggestionsActive: true });
    this.setState({ overlayClick: true });
  }

  onClickOverlay() {
    this.setState({ suggestionsActive: false });
    this.setState({ overlayClick: false });
  }

  clearInput() {
    this.setState({ suggestionsActive: false });
    this.state.query.value = ''
  }

  render() {
    if (!this.state.query.value) {
      this.state.results = []
    }
    let showResults

    if (this.state.results.length && this.state.suggestionsActive) {

      showResults =
        <div className="suggestions " onClick={() => this.onClickOverlay()}>
          {this.state.results.map((result, index) => {
            var text = result.excerpt.rendered.replace(/<\/?[^>]+>/ig, " ");
            var title = result.title.rendered.replace(/<\/?[^>]+>/ig, " ");
            if (index < 4) {
              return (
                <div className="item" key={index}
                  onClick={() => this.goToPost(result.slug)} >
                  {<div className="title" dangerouslySetInnerHTML={{ __html: title }} />}
                  {<div className="info" dangerouslySetInnerHTML={{ __html: text }} />}
                </div>
              );
            } else {
              if (this.props.path === '/how-it-works') {
                return (
                  <div className="item" key={index} 
                  onClick={() => this.goToResults(this.state.query.value)}>
                    <div className="title" >
                      More Option  <Icon.ChevronRight size={10} />
                    </div>
                  </div>
                )
              }
              
            }

          })}
        </div>
      //}, 1000);
    }

    let overlay
    if (this.state.overlayClick && this.state.results.length) {
      overlay =
        <div onClick={() => this.onClickOverlay()} className="overlaySuggestions "></div>
    }


    let classResults = ""
    if (this.state.results.length) {
      if (this.state.suggestionsActive) {
        classResults = "results"
      }
    }

    return (
      <section id="lmi-search-support" className="search-howto">
        <div className="bg-search-support">
          <img src={img_header_md1} />
          <img src={img_header_sm1} />
        </div>
        <div className="overlay"></div>
        <div className="info-search-support">
          <div className="content-search-support">
            <h1 className="text-center">{this.state.textTittle}</h1>
            <p className="text-center">How can we help you?</p>
            <div className="icon-input" >
              <input autoComplete="off"
                className={classResults}
                id="idInput" type="text"
                placeholder="Type your question"
                onClick={() => this.onClickInput()}
                ref={input => this.state.query = input}
                onChange={() => this.textSearch()} />
              <Icon.Search size={18} />
            </div>
            {showResults}
            {overlay}
          </div>
        </div>
      </section>
    );
  }

}

export default withRouter(searchSupport);