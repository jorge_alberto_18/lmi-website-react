import React, { Component } from "react";
import img_video from '../../img/video-min.jpg';
import icon_play from '../../img/play.svg';

import ModalVideo from 'react-modal-video'

import "./video.scss";
class videoModal extends Component  {

  constructor () {
    super()
    this.state = {
      isOpen: false
    }
    this.openModal = this.openModal.bind(this)
  }

  openModal () {
    this.setState({isOpen: true})
  }

  render () {
    return (
      <section id="lmi-how-it-works" className="video lmi-animation">
      <img className="img-video" src={img_video} alt="bg video" />
      <div className="text lmi-animation">
        <small>Here's</small>
        <span>How it works</span>
      </div>
      <div className="btn-play lmi-animation">
        <button onClick={this.openModal} className="btn launch-modal"  >
          <img src={icon_play} className="icon-play" alt="icon play" />
        </button>
      </div>
      <ModalVideo 
        channel='vimeo' 
        isOpen={this.state.isOpen} 
        videoId='373655260' 
        title='false'
        onClose={() => this.setState({isOpen: false})} />
        
      </section>
    )
  }
}

// const Video = (props) => {
//     const { buttonLabel, className} = props;
//     const [modal, setModal] = useState(false);
//     const toggle = () => setModal(!modal);

//     return (
//       <section id="lmi-how-it-works" className="video lmi-animation">
//         <img className="img-video" src={img_video} />
//         <div className="text lmi-animation">
//           <small className="">Here's</small>
//           <span>How it works</span>
//         </div>
//         <div className="btn-play lmi-animation">
//           <button onClick={toggle} className="btn launch-modal"  >
//             <img src={icon_play} className="icon-play" />
//           </button>
//         </div>
//           <Modal isOpen={modal} toggle={toggle} className={className} className="custom-modal" id="modal-video"> 
//             <ReactPlayer 
//               className='react-player'
//               url='https://player.vimeo.com/video/373655260?title=0&byline=0=0&api=1'  
//               width='100%'
//               height='100%'
//             />
//           </Modal>
//       </section>
//     );
// }



export default videoModal;