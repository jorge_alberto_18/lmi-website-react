import React, { Component } from "react";

import circle from '../../img/cards-why/check-circle.svg';
import economical from '../../img/cards-why/economical.svg';
import safe from '../../img/cards-why/safe.svg';
import secure from '../../img/cards-why/secure.svg';

import "./why.scss";

class Why extends Component {
  render() {
    return (
      <section id="lmi-why" className="why lmi-animation">
      <div className="content-main ">
          <div className="subtitle lmi-animation">
              Why <span>lend me it?</span>
          </div>
          <p className="text-why lmi-animation">
              Lend Me It is a peer-to-peer app designed for lending and renting anything and everything within a secure environment.
          </p>
          <p className="text-why lmi-animation">
              Have a bike you rarely use? Instead of selling it once, create a listing with photos, set a daily lending price and start making extra money.
              Need a tool for the day but don’t want to buy one? Search for it within your area, book the rental, make a friend and save money.
          </p>
          <p className="text-why lmi-animation">
              Protect the environment through community sharing.
          </p>
          <div className="cards-why">
              <div className="card lmi-animation">
                  <div className="card-icon">
                      <img src={economical} alt="Icon Economical" />
                  </div>
                  <div className="card-name">Economical</div>

                  <div className="card-description">
                      Earn money from items you already own. Save by not buying things you rarely use.
                  </div>
              </div>
              <div className="card lmi-animation">
                  <div className="card-icon">
                      <img src={safe} alt="Icon Safe" />
                  </div>
                  <div className="card-name">Safe</div>

                  <div className="card-description">
                      ID verification and privacy safeguards help to make Lending and borrowing safe for both parties.
                  </div>
              </div>
              <div className="card lmi-animation">
                  <div className="card-icon">
                      <img src={secure}  alt="Icon Secure"/>
                  </div>
                  <div className="card-name">Secure</div>

                  <div className="card-description">
                      Best in class payment processing by Stripe keeps your financial details secure. 
                  </div>
              </div>
              <div className="card lmi-animation">
                  <div className="card-icon">
                      <img src={circle}  alt="Icon Guaranteed"/>
                  </div>
                  <div className="card-name">Guaranteed</div>

                  <div className="card-description">
                      We guarantee you’ll get your items back safely, or we’ll pay up to $3000 for replacement or repair.
                  </div>
              </div>
          </div>
      </div>
  </section>
    );
  }
}


export default Why;