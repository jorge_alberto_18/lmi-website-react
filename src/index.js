/* eslint-disable no-undef */
import React from "react";
import ReactDOM from "react-dom"; 
import App from "./router";

import 'bootstrap/dist/css/bootstrap.min.css';
import "./css/index.scss";

ReactDOM.render(<App/>, document.getElementById("root"));

