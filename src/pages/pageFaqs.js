import React, { Component } from "react";
import Navbar from "../components/navbar/navbar";
import Faq from "../components/faq/faq";

import Footer from "../components/footer/footer";
import Search from "../components/search/search";
import MenuLinks from "../components/menu-links/menuLinks";

class Support extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      idCatFaq: 2
    };
  }
  getData = (query) => {
      this.setState({
        search: query
      })
    }

  render() {
    const { path } = this.props.match;
    return (
      <div className='faqs'>
        <Navbar/>
        <Search path={path} idCatParent={this.state.idCatFaq} ></Search>
        <main>
          <Faq path={path} onChange={this.getData.bind(this)}  search={this.state.search}  />
        </main>
        <MenuLinks  path={path} />
        <Footer />
      </div>
    );
  }
}

export default Support;