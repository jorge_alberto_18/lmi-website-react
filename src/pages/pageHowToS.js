import React, { Component } from "react";
import {  Route, Switch, Link,  } from 'react-router-dom';
import * as Icon from 'react-feather';

import Navbar from "../components/navbar/navbar";
import Footer from "../components/footer/footer";
import Search from "../components/search/search";
import MenuLinks from "../components/menu-links/menuLinks";

import "../css/pageHowTos.scss";

class Support extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idCatHowto: 4, idCatActive: '', pageSlug: '', activeLinkCat: '',
      categories: [], posts: [],
      isLoading: true
    };
  }

  async componentDidMount() {
    const fetchCat = await (await fetch('https://lendmeit.com/support/lmisuppla/wp-json/wp/v2/categories?hide_empty=true&per_page=20')).json()
    this.setState({
      categories: fetchCat
    })

    const fetchPost = await (await fetch('https://lendmeit.com/support/lmisuppla/wp-json/wp/v2/posts?&per_page=100&categories=' + this.state.idCatHowto + '&orderby=id&order=asc')).json()
    this.setState({
      posts: fetchPost
    })

  }

  changeSub(id, index) {
    this.setState({
      idCatActive: id
    });
    this.setState({ activeLinkCat: index });
  }

  handleChangeSub = (id) => {
    this.state.idCatActive = id
  }

  render() {
    const { path } = this.props.match;
    const { categories, posts, activeLinkCat, idCatHowto, idCatActive } = this.state;
    let letActive = activeLinkCat
    var fisrtCat = true
    // categories.sort((a, b) => b - a).map(cat => {
    //   if (cat.parent == idCatHowto && idCatActive == '' && fisrtCat) {
    //     fisrtCat = false
    //     this.state.idCatActive = cat.id
    //   }
    // });


    return (
      <div className="howto-page">
        <Navbar />
        <Search path={path} idCatParent={idCatHowto} ></Search>
        <main>
          <div className="content-support">
            <section className="support">
              <div className="wp-categories">
                {categories.map((cat, index) => {
                  if (cat.parent === idCatHowto) {
                    if (fisrtCat){
                      this.state.idCatActive = cat.id
                      fisrtCat = false;
                    }
                    return (
                      <div className="cLink" key={index}>
                        <Link to="/how-it-works" onClick={(e) => this.changeSub(cat.id, index)}
                          {...letActive === -1 ? letActive = index : ''}
                          className={letActive === index ? 'link-cat active' : 'link-cat'}>
                          {cat.name} </Link>
                          {console.log(cat.name)}
                      </div>
                    )
                  }
                })}

              </div>
              <div className="wp-posts">
                <Switch>
                  <Route exact path={path} render={(props) => <Index {...props}
                    idCatActive={idCatActive}
                    idCatHowto={idCatHowto}
                    categories={categories}
                    posts={posts} />}
                  />

                  <Route path="/how-it-works/cat/:catSlug" render={(props) => <Cat {...props}
                    posts={posts}
                    categories={categories} />}
                  />

                  <Route path="/how-it-works/post/:postSlug" render={(props) => <Post {...props}
                    idCatHowto={idCatHowto}
                    posts={posts}
                    categories={categories}
                    handlePost={this.handleChangeSub.bind(this)}
                  />}
                  />
                  <Route path="/how-it-works/search" render={(props) => <Results {...props}
                    idCatParent={idCatHowto}
                    posts={posts}
                    categories={categories} />}
                  />
                </Switch>
              </div>
            </section>
          </div>
        </main>
        <MenuLinks path={path}/>
        <Footer />
      </div>
    );
  }
}

const Index = (props) => {
  const { path } = props.match;
  // console.log(props.location.pathname.split('\\').pop().split('/').pop())
  if (props.idCatActive) {
    return (
      props.categories.map((sub, index) => {
        let count = 1, plus = false
        if (sub.parent === props.idCatActive) {
          return (
            <div key={index} className="wp-post">
              <h4>{sub.name}</h4>
              {props.posts.map((post, index) => {
                if (count <= 3) {
                  return post.categories.map(cat => {
                    if (cat === sub.id) {
                      count++
                      return (
                        <div className='link-post' key={index} >
                          <Link to={path + `/post/` + post.slug}
                            dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
                        </div>
                      )
                    }
                  });
                } else {
                  if (!plus && sub.count > 3) {
                    plus = true;
                    return (
                      <div className='link-post' key={index}>
                        <Link to={path + `/cat/` + sub.slug} >More<Icon.ChevronRight size={18} /></Link>
                      </div>
                    )
                  }
                }
              })
              }
            </div>
          )
        }

      })
    );
  } else {
    return (
      <div className="blur">
        <div className="loader"></div>
      </div>
    )
  }


}

const Cat = (props) => {
  // const { path } = props.match;
  var slug = props.location.pathname.split('\\').pop().split('/').pop();

  return (
    props.categories.map((sub, index) => {
      if (sub.slug === slug) {
        return (
          <div key={index} className="wp-post">
            <h4>{sub.name}</h4>
            {props.posts.map((post, index) => {
              return post.categories.map(cat => {
                if (cat === sub.id) {
                  return (
                    <div className='link-post' key={index} >
                      <Link to={'/how-it-works/post/' + post.slug}
                        dangerouslySetInnerHTML={{ __html: post.title.rendered }}
                      />
                    </div>
                  )
                }
              });
            })
            }
          </div>
        )
      }

    })
  )

};


const Post = (props) => {
  // const { path } = props.match;
  var page = props.location.pathname.split('\\').pop().split('/').pop();
  var postCategoryName = ""
  var postCategoryId = ""
  // var postCategorySlug = ""
  var postSubCategoryName = ""
  var postSubCategorySlug = ""

  // Breadcrumbs
  props.posts.map(post => {
    if (post.slug === page) {
      post.categories.map((postCat, i) => {
        props.categories.map(propsCategories => {
          if (propsCategories.id === postCat && props.idCatHowto === propsCategories.parent) {
            postCategoryId = propsCategories.id;
            postCategoryName = propsCategories.name;
            // postCategorySlug = propsCategories.slug;
          }
        })
      })
      post.categories.map((postCat, i) => {
        props.categories.map(propsCategories => {
          if (propsCategories.id === postCat && postCategoryId === propsCategories.parent) {
            postSubCategoryName = propsCategories.name;
            postSubCategorySlug = propsCategories.slug;
          }
        })
      })
    }
  });


  if (props.posts.length) {
    return (
      props.posts.map((post, index) => {
        if (post.slug === page) {
          return (
            <div key={index}>
              <div className='wp-render'>
                <div className="breadcrumbs">
                  <div className='tagName'>
                    <span>  <Link to={`/how-it-works`} onClick={() => props.handlePost(postCategoryId)}>{postCategoryName}</Link></span>
                    <Icon.ChevronRight size={10} />
                  </div>
                  <div className='tagName'>
                    <span> <Link to={`/how-it-works/cat/` + postSubCategorySlug} >{postSubCategoryName}</Link></span>
                    <Icon.ChevronRight size={10} />
                  </div>
                  <div className='tagName'>
                    <span><b>{post.title.rendered}</b></span>
                  </div>
                </div>
                <h1 dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
                <div id='wp-content-render' className="content-rendered" dangerouslySetInnerHTML={{ __html: post.content.rendered }} />
              </div>
            </div>
          )
        }
      })
    )
  }
  else {
    return (
      <div className="blur">
        <div className="loader"></div>
      </div>
    )

  }

};

// const Results = (props) => {
//   const { path } = props.match;
//   var search = props.location.pathname.split('\\').pop().split('/').pop();

//   return (
//   <div>{search}</div>
//   )

// };

class Results extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: []
    }
    this.textSearch();
  }
  textSearch() {
    var search = this.props.location.pathname.split('\\').pop().split('/').pop();
    var replaced = search.split('-').join('%20');

    fetch('https://lendmeit.com/support/lmisuppla/wp-json/wp/v2/posts?orderby=id&order=asc&per_page=100&categories=' + this.props.idCatParent + '&search=' + replaced)
      .then(data => data.json())
      .then(result => {
        this.setState({
          results: result
        })
      }).catch(data => console.log(data))

  }
  goToPost(slug){
    this.props.history.push("/how-it-works/post/" + slug)
  }

  render() {
  var search2 = this.props.location.pathname.split('\\').pop().split('/').pop();
  var replaced2 = search2.split('-').join(' ');
    return (
      <div className="box">
         <div className="item mb5">
            <b className="title">SEARCH RESULTS</b>
            <p className="info">Four results for “{replaced2}"</p>
          </div>
        {this.state.results.map((result, index) => {
          var text = result.excerpt.rendered.replace(/<\/?[^>]+>/ig, " ");
          var title = result.title.rendered.replace(/<\/?[^>]+>/ig, " ");
          return (
            <div className="item" key={index}>
              {<b onClick={() => this.goToPost(result.slug)}  className="title" dangerouslySetInnerHTML={{ __html: title }} />}
              {<p className="info" dangerouslySetInnerHTML={{ __html: text }} />}
            </div>
          )
        })}
      </div>
    );
  }
}



export default Support;