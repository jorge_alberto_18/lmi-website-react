import React, { Component } from "react";
import NavbarSearch from "../components/navbarSearch/navbarSearch";
import ItemDetails  from "../components/item-detail/item-detail";
import Footer from "../components/footer/footer";
import MenuLinks from "../components/menu-links/menuLinks";


function PageItemsDetails(props) {
  const { path } = props.match;


  return (
    <div className="search-page">
      <NavbarSearch />
      <main>
        <ItemDetails/>
        <MenuLinks path={path} />
      </main>
      <Footer />
    </div>
  );
}

export default PageItemsDetails;
