import React, { Component, Suspense } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import NavbarSearch from "../components/navbarSearch/navbarSearch";
import Header from "../components/header/header";
import Carrusel from "../components/carrusel/carrusel";
import MenuLinks from "../components/menu-links/menuLinks";
import Covid19 from '../components/covid19/covid19'
import Geocode from 'react-geocode';

const Video = React.lazy(() => import('../components/video/video'));
const Why = React.lazy(() => import('../components/why/why'));
const Map = React.lazy(() => import('../components/map/map'));
const Email = React.lazy(() => import('../components/email/email'));
const Footer = React.lazy(() => import('../components/footer/footer'));


class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemHeader: 1,
      Header: false,
      itemCarousel: 1,
      Carousel: false,
      isMobile: false,
      location: [],
    };

    this.fnGetLocation = this.fnGetLocation.bind(this);
  }
  componentWillMount() {
    if (window.innerWidth <= 768) {
      this.setState({ isMobile: true });
    }
  }

 componentDidMount() {
    // this.inViewport();

    let self = this;
    Geocode.setApiKey('AIzaSyBTGEl38pnb8JAaPpTJaFGngss8P7MUW4c');
    Geocode.setLanguage('en');
    Geocode.fromLatLng('34.0778324', '-118.6861442')
          .then(
            json => {
              // var formattedAddress = json.results[0].formatted_address;

              var addresCount = json.results[0].address_components.length;

              var city = json.results[0].address_components[addresCount - 3].long_name
                  var state = json.results[0].address_components[addresCount - 2].long_name
                  var country = json.results[0].address_components[addresCount - 1].long_name

              self.fnGetLocation([
                '34.0778324', 
                '-118.6861442',
                city + ", " + state + ", " + country]);
            },
            error => {
              console.error(error);
            }
          )

    if("geolocation" in navigator) {
      console.log("GEOLOCALIZACION DISPONIBLE");
      navigator.geolocation.getCurrentPosition(function(position) {
        Geocode.setApiKey('AIzaSyBTGEl38pnb8JAaPpTJaFGngss8P7MUW4c');
        Geocode.setLanguage('en');
        Geocode.fromLatLng(position.coords.latitude, position.coords.longitude)
              .then(
                json => {
                  //var formattedAddress = json.results[0].formatted_address;

                  var addresCount = json.results[0].address_components.length;
                  
                  var city = json.results[0].address_components[addresCount - 3].long_name
                  var state = json.results[0].address_components[addresCount - 2].long_name
                  var country = json.results[0].address_components[addresCount - 1].long_name

                  self.fnGetLocation([
                    position.coords.latitude.toString(), 
                    position.coords.longitude.toString(),
                    city + ", " + state + ", " + country]);
                },
                error => {
                  console.error(error);
                }
              )
          })
    }
    
    window.addEventListener('scroll', this.inViewport);
    window.addEventListener('scroll', this.parallax);
  }

  fnGetLocation (loc) {
    this.setState({location: loc});
  }
  // fnGetUserIP() {
  //   var request = new XMLHttpRequest()
  //   var ipAddress = ''
  //   request.open('GET', "https://api.ipify.org?format=jsonp=", true)
  //   var ipAddress = request.onload = function() {
  //     //// debugger;
  //     if (request.status >= 200 && request.status < 400) {
  //       // Success!
  //       //alert(request.responseText);
  //       ipAddress = request.responseText

  //       console.log(ipAddress);
  //       // fnGetIpLocationDetails(ipAddress);
  //     } else {
  //       // We reached our target server, but it returned an error
  //       console.log("We reached our target server, but it returned an error")
  //     }
  //   }
  //   request.onerror = function() {
  //   // There was a connection error of some sort
  //   console.log("There was a connection error of some sort")
  //   }
  //   request.send()
  // }

  // fnGetIpLocationDetails (ip) {
  //   try{
  //    //// debugger;
  //    var ipLocationDetails;
  //    var ip_address_det = "https://api.ipdata.co/?api-key=9a0a40e7ba59aa551a8843fe35a2e65d1ab2bbdde1536eaf26516bc6";
  //    $.ajax ({
  //      type: "GET",
  //      url: ip_address_det,
  //      async: false,
  //      success: function(response) {  
  //        console.log("ip details");
  //        console.log(response);
  //        ipLocationDetails = response;
  //      },
  //      error: function(resp){
  //        //ipLocationDetails = "";
  //      }
  //    });
  //   }
  //   catch(err)
  //   {
  //      //// debugger;
  //   }
  //    return ipLocationDetails;
  //  }



  parallax() {
    var scrolled = window.scrollY;
    const markerTop = Array.from(document.getElementsByClassName('parrallaxTop'));
    markerTop.forEach(item => {
      item.style.top = 'calc(50% - ' + (scrolled * .2) + 'px)';
    });

  }

  inViewport() {
    var event = window.innerHeight;
    const animationArray = Array.from(document.getElementsByClassName('lmi-animation'));
    animationArray.forEach(item => {
      var divPos = item.offsetTop;
      var topOfWindow = window.scrollY;
      if (divPos < topOfWindow + event) {
        if (item.id === 'lmi-carrusel' && topOfWindow <= 480) {
          setTimeout(() => {
            item.classList.add('lmi-show');
          }, 1000);
        } else {
          item.classList.add('lmi-show');
        }
      }
    });
  }

  readyHeader() {
    this.setState({ Header: true });
    this.inViewport();
  }

  readyCarousel() {
    console.log('ready');
    this.setState({ itemCarousel: this.state.itemCarousel + 1 });
    if (this.state.itemCarousel === 4) {
      this.setState({ Carousel: true });
    }
  }

  render() {
    const { path } = this.props.match;
    const renderLoader = () => <p>Loading</p>;
    let mainSecctions
    let footerSecctions
    
    //if (this.state.Header && this.state.Carousel) {
      mainSecctions =
        <div>
          <Suspense fallback={renderLoader()}>
            <Video />
            <Why />
            <Map />
            <Email />
          </Suspense>
          <MenuLinks path={path} />
        </div>
      footerSecctions =
        <div>
          <Suspense fallback={renderLoader()}>
            <Footer />
          </Suspense>
          <Covid19 showCovid={true}></Covid19>
        </div>

    //}
    return (
      <div className="lmi-animated">
        <NavbarSearch fnGetLocation={this.fnGetLocation} currentLocation={this.state.location[2]}/>
        <Header onLoadBanner={this.readyHeader.bind(this)} isMobile={this.state.isMobile} start={this.state.Carousel} />
        <main>
          <Carrusel location={this.state.location} />
          {mainSecctions}
        </main>
        {footerSecctions}
      </div>
    );
  }
}


export default Main;