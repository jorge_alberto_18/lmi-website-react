import React, { Component } from "react";
import Navbar2 from "../components/navbar2/navbar2";
import NoPage from "../components/no-page/no-page";
import Footer from "../components/footer/footer";

class external extends Component {
  render() {
    return (
      <div>
        <Navbar2 />
        <main className="content-terms">
          <NoPage /> 
        </main>
        <Footer />
      </div>
    );
  }
}

export default external;