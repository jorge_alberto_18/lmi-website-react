import React, { Component } from "react";
import ContentResults from "../components/content-results/content-results";
import NavbarSearch from "../components/navbarSearch/navbarSearch";
import Footer from "../components/footer/footer";
import MenuLinks from "../components/menu-links/menuLinks";

class PageResults extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { path } = this.props.match;

    return (
      <div className="search-page">
        {console.log(this.props.location.pathname)}
      <NavbarSearch />
        <main>
            <ContentResults />
            <MenuLinks path={path} />
        </main>
        <Footer />
      </div>
      
    );
  }
}

export default PageResults;
