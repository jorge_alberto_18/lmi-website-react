import React, { Component } from "react";
import Navbar2 from "../components/navbar2/navbar2";
import Page from "../components/ext-url/extUrl";

import Footer from "../components/footer/footer";
import SearchSupport from "../components/search-support/search-support";

class Support extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
  }
  getData = (query) => {
      this.setState({
        search: query
      })
    }

  render() {
    const path = this.props.location
    return (
      <div>
        <Navbar2/>
        <main className="content-terms">
          <Page path={path} />
        </main>
        <Footer />
      </div>
    );
  }
}

export default Support;