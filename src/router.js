import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { isAndroid, isIOS } from 'react-device-detect';

import Main from './pages/pageMain';
import Support from './pages/pageSupport';
import Faqs from './pages/pageFaqs';
import HowTos from './pages/pageHowToS';
import NotFound from './pages/pageNotFound';
// import Test from './pages/pageTest';
import Privacy from './pages/pagePrivacy';
import PageResults from './pages/pageResults';
import PageItemsDetails from './pages/pageItemDetails';


class Install extends Component {
  render() {
    if (isAndroid) {
      window.location.href = 'https://play.google.com/store/apps/details?id=com.lendmeit.app';
    }
    if (isIOS) {
      window.location.href = 'https://apps.apple.com/us/app/lend-me-it-lend-rent-easily/id1444352676';
    }
    return <Redirect to='/' />
  }
}

// <BrowserRouter basename='/test/carousel/'>
class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/faq" component={Faqs} />
            <Route exact path="/terms" component={Support} />
            <Route exact path="/privacy-policy" component={Support} />
            <Route exact path="/eula" component={Support} />
            <Route exact path="/about" component={Support} />
            <Route exact path="/guarantee" component={Support} />
            <Route exact path="/install" component={Install} />
            <Route path="/how-it-works" component={HowTos} />
            <Route exact path="/privacy" component={Privacy} />
            <Route path="/how-it-works" component={HowTos} /> 
            <Route path="/search" component={PageResults} />
            <Route path="/item" component={PageItemsDetails} />

            /* SIEMPRE AL FINAL */
            <Route path="*" component={NotFound} />
            {/* <Route path="/test" component={Test} />        */}
          </Switch>
        </Router>
        
      </div>
    );
  }
}
export default App;